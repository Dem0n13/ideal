﻿using System;
using IdeAl.Communication.Common;

namespace IdeAl.Service.Core.DebuggerService
{
    internal static class VarTypeExtensions
    {
        public static string ToCType(this VarType type)
        {
            switch (type)
            {
                case VarType.Auto:
                    return string.Empty;
                case VarType.Register:
                    return "$";
                case VarType.Int8:
                    return "(char)";
                case VarType.Int16:
                    return "(short)";
                case VarType.Int32:
                    return "(long)";
                case VarType.Int64:
                    return "(long long)";
                case VarType.Float32:
                    return "(float)";
                case VarType.Float64:
                    return "(double)";
                case VarType.Float80:
                    return "(long double)";
                case VarType.String:
                    return "(char*)&";
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }
    }
}
using System;
using Antlr4.Runtime.Tree;

namespace IdeAl.Service.Core.DebuggerService.Parsing
{
    public static class AntrlExtensions
    {
        public static T As<T>(this ITerminalNode node)
        {
            var text = node == null ? null : node.GetText();
            var type = typeof (T);
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>)
                ? (string.IsNullOrEmpty(text) ? default(T) : (T) Convert.ChangeType(text, type.GetGenericArguments()[0]))
                : (T) Convert.ChangeType(text, type);
        }
    }
}
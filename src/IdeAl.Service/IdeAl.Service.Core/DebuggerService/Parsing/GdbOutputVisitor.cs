﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Antlr4.Runtime;
using IdeAl.Communication.Output.Gdb;

namespace IdeAl.Service.Core.DebuggerService.Parsing
{
    public sealed class GdbOutputVisitor : GdbOutputBaseVisitor<object>, IGdbOutputParser
    {
        public GdbEvent Parse(string input)
        {
            if (input == null) return null;

            var inputStream = new AntlrInputStream(input);
            var lexer = new GdbOutputLexer(inputStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new GdbOutputParser(tokenStream);
            var output = parser.output();
            return (GdbEvent) VisitOutput(output);
        }

        public override object VisitResult_record(GdbOutputParser.Result_recordContext context)
        {
            return new ResultRecord(
                context.TOKEN().As<long?>(),
                context.result_class().GetText(),
                context.result()
                    .Select(VisitResult)
                    .Cast<KeyValuePair<string, GdbValue>>()
                    .ToDictionary(pair => pair.Key, pair => pair.Value));
        }
        
        public override object VisitStream_record(GdbOutputParser.Stream_recordContext context)
        {
            return new StreamRecord((string) base.VisitStream_record(context));
        }

        public override object VisitStream_message(GdbOutputParser.Stream_messageContext context)
        {
            return ((string) VisitC_string(context.c_string())).Trim();
        }

        public override object VisitResult(GdbOutputParser.ResultContext context)
        {
            return new KeyValuePair<string, GdbValue>(
                context.VARIABLE().GetText(),
                (GdbValue) VisitValue(context.value()));
        }

        public override object VisitConst(GdbOutputParser.ConstContext context)
        {
            return new GdbValue((string) VisitC_string(context.c_string()));
        }

        public override object VisitTuple(GdbOutputParser.TupleContext context)
        {
            return new GdbValue(
                context.children
                    .OfType<GdbOutputParser.ResultContext>()
                    .Select(VisitResult)
                    .Cast<KeyValuePair<string, GdbValue>>()
                    .ToDictionary(pair => pair.Key, pair => pair.Value));
        }

        public override object VisitList(GdbOutputParser.ListContext context)
        {
            return new GdbValue(
                context.children
                    .Select(Visit)
                    .Where(o => o != null)
                    .Cast<GdbValue>()
                    .ToArray());
        }

        public override object VisitC_string(GdbOutputParser.C_stringContext context)
        {
            var text = context.GetText();
            Debug.Assert(text.Length >= 2);
            return Regex.Unescape(text.Substring(1, text.Length - 2));
        }

        public override object VisitTerminator_record(GdbOutputParser.Terminator_recordContext context)
        {
            return new TerminatorRecord();
        }
    }
}

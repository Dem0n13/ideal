﻿using IdeAl.Communication.Output.Gdb;

namespace IdeAl.Service.Core.DebuggerService.Parsing
{
    public interface IGdbOutputParser
    {
        GdbEvent Parse(string input);
    }
}
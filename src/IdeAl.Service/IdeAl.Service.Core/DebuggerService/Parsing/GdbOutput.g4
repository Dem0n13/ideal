grammar GdbOutput;

output : result_record | stream_record | terminator_record;
result_record : TOKEN? ('^' | '*' | '+' | '=' ) result_class (',' result)*;
result_class : VARIABLE;
result : VARIABLE '=' value;
value : const
	| tuple
	| list;
const : c_string;
c_string : QUOATED_STRING;
tuple : '{}'
	| '{' result (',' result)* '}';
list : '[]'
	| '[' value (',' value)* ']'
	| '[' result (',' result)* ']';
stream_record : ('~' | '@' | '&') stream_message;
stream_message : c_string;
terminator_record : '(gdb)';

QUOATED_STRING : '"' (~('"' | '\\') | '\\' ('"' | '\\' | 'r' | 'n'))*? '"';
TOKEN : [0-9]+;
VARIABLE : [a-z][a-z-_]+;

WS : ' ' -> skip;

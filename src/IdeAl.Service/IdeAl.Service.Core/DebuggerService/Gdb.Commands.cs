﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using IdeAl.Communication.Common;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;

namespace IdeAl.Service.Core.DebuggerService
{
    public partial class Gdb
    {
        #region Commands

        public void Start()
        {
            this.ThrowIfDisposed();

            EnqueueCommand("-exec-run");
        }

        public void StepInto()
        {
            this.ThrowIfDisposed();

            EnqueueCommand("-exec-step");
        }

        public void StepOver()
        {
            this.ThrowIfDisposed();

            EnqueueCommand("-exec-next");
        }

        public void Continue()
        {
            this.ThrowIfDisposed();

            EnqueueCommand("-exec-continue");
        }

        [Obsolete("Stop command is not supported. Use Dispose method instead", true)]
        public void Stop()
        {
            throw new NotSupportedException("Stop command is not supported. Use Dispose method instead");
        }

        public void AddBreakpoint(int lineNumber)
        {
            this.ThrowIfDisposed();

            AddBreakpoint(lineNumber.ToString(CultureInfo.InvariantCulture));
        }

        public void AddBreakpoint(string label)
        {
            this.ThrowIfDisposed();
            label.ThrowIfNullOrWhiteSpace("label");
            
            EnqueueCommand("-break-insert " + label, AddBreakpointResultHandler);
        }

        public void DeleteBreakpoint(int number)
        {
            this.ThrowIfDisposed();

            var command = string.Format("-break-delete {0}", number);
            EnqueueCommand(command, result => DeleteBreakpointResultHandler(result, number));
        }

        public void Watch(RegisterGroup registerGroup)
        {
            this.ThrowIfDisposed();
            registerGroup.ThrowIfUnknown("registerGroup");

            var registerGroupStr = registerGroup.ToString();
            var registerStrs = _registers.Where(pair => pair.Value == registerGroup).Select(pair => pair.Key).ToArray();
            AddEvent(new ResultRecord("reg_list", new Dictionary<string, GdbValue>
                                                  {
                                                      {"group", new GdbValue(registerGroupStr)},
                                                      {"list", GdbValue.From(registerStrs)}
                                                  }));
            foreach (var registerStr in registerStrs)
                Watch(registerStr, VarType.Register);
        }

        public void Watch(string name, VarType type)
        {
            this.ThrowIfDisposed();
            name.ThrowIfNullOrWhiteSpace("name");
            type.ThrowIfUnknown("type");

            var cType = type.ToCType();
            EnqueueCommand("-var-create " + name + " * " + cType + name, WatchResultHandler);
        }

        public void Unwatch(RegisterGroup registerGroup)
        {
            this.ThrowIfDisposed();
            registerGroup.ThrowIfUnknown("name");

            var registerStrs = _registers.Where(pair => pair.Value == registerGroup).Select(pair => pair.Key).ToArray();
            foreach (var registerStr in registerStrs)
                Unwatch(registerStr);
        }

        public void Unwatch(string name)
        {
            this.ThrowIfDisposed();
            name.ThrowIfNullOrWhiteSpace("name");

            var command = string.Format("-var-delete {0}", name);
            EnqueueCommand(command, result => UnwatchResultHandler(result, name));
        }

        public void SetValue(string key, string value)
        {
            this.ThrowIfDisposed();
            key.ThrowIfNullOrWhiteSpace("key");
            value.ThrowIfNullOrWhiteSpace("key");

            var command = string.Format("-var-assign {0} {1}", key, value);
            EnqueueCommand(command, SetValueResultHandler);
        }

        private void UpdateWatched()
        {
            const string command = "-var-update --all-values *";
            RunCommandAsync(command, UpdateWatchedResultHandler);
        }

        #endregion

        #region Handlers

        private void AddBreakpointResultHandler(ResultRecord result)
        {
            if (result.Class == "done")
                result = new ResultRecord(result.Token, "break_inserted", result.Result);
            PermitCommandExecuting();
            AddEvent(result);
        }

        private void DeleteBreakpointResultHandler(ResultRecord result, int number)
        {
            if (result.Class == "done")
            {
                result = new ResultRecord(result.Token, "break_deleted", result.Result);
                result.Result.Add("number", new GdbValue(number.ToString(CultureInfo.InvariantCulture)));
            }
            PermitCommandExecuting();
            AddEvent(result);
        }

        private void WatchResultHandler(ResultRecord result)
        {
            if (result.Class == "done")
                result = new ResultRecord(result.Token, "var_created", result.Result);
            PermitCommandExecuting();
            AddEvent(result);
        }

        private void UnwatchResultHandler(ResultRecord result, string name)
        {
            if (result.Class == "done")
            {
                result = new ResultRecord(result.Token, "var_deleted", result.Result);
                result.Result.Add("name", new GdbValue(name));
            }
            PermitCommandExecuting();
            AddEvent(result);
        }

        private void SetValueResultHandler(ResultRecord result)
        {
            if (result.Class == "done")
                UpdateWatched();
            PermitCommandExecuting();
        }

        private void UpdateWatchedResultHandler(ResultRecord result)
        {
            if (result.Class == "done")
                result = new ResultRecord(result.Token, "vars_updated", result.Result);
            AddEvent(result);
        }

        private void SaveRegistersInfo(ResultRecord record)
        {
            var result = record.Result["register-names"];
            var values = result.Value<GdbValue[]>();
            var registerNames = values.Select(value => value.Value<string>())
                .Where(name => !string.IsNullOrEmpty(name));

            foreach (var registerName in registerNames)
                _registers[registerName] = Registers.DetermineGroup(registerName);
        }

        #endregion
    }
}
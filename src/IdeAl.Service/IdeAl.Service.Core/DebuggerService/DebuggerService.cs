﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using IdeAl.Communication.Common;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;
using NLog;

namespace IdeAl.Service.Core.DebuggerService
{
    public class DebuggerService : IDisposableEx
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ConcurrentDictionary<long, DebugSession> _sessions = new ConcurrentDictionary<long, DebugSession>();

        private readonly TimeSpan _defaultSessionTimeout = new TimeSpan(0, 15, 0);
        private const double UnusedSessionCollectorInterval = 5*1000.0;
        private readonly Timer _unusedSessionCollectorTimer;

        public DebuggerService()
        {
            _unusedSessionCollectorTimer = new Timer(UnusedSessionCollectorInterval) {AutoReset = false};
            _unusedSessionCollectorTimer.Elapsed += (sender, args) => CollectUnusedSessions();
            _unusedSessionCollectorTimer.Enabled = true;
        }

        public long StartSession(string directory, string srcFileName)
        {
            this.ThrowIfDisposed();
            directory.ThrowIfNull("directory");
            srcFileName.ThrowIfNullOrWhiteSpace("srcFileName");

            srcFileName = Path.GetFileName(srcFileName);
            var srcPath = Path.Combine(directory, srcFileName);
            var exePath = Path.ChangeExtension(srcPath, "exe");

            var session = new DebugSession(srcPath, exePath);
            _sessions.TryAdd(session.Id, session);
            return session.Id;
        }

        public void StopSession(long id)
        {
            DebugSession deleted;
            if (_sessions.TryRemove(id, out deleted))
                deleted.Dispose();
        }

        public void Execute(long id, Action<IDebugger> action)
        {
            this.ThrowIfDisposed();
            action.ThrowIfNull("action");

            DebugSession session;
            if (!_sessions.TryGetValue(id, out session))
                throw new KeyNotFoundException("Debug session not found");

            session.Invoke(action);
        }

        public Dictionary<long, GdbEvent[]> GetEvents()
        {
            this.ThrowIfDisposed();

            return _sessions
                .Where(pair => pair.Value.HasEvents)
                .ToDictionary(pair => pair.Key, pair => pair.Value.GetEvents());
        }

        private void CollectUnusedSessions()
        {
            Logger.Trace("CollectUnusedSessions is started");

            foreach (var session in _sessions.Values)
            {
                if (DateTime.Now - session.LastActionTime > _defaultSessionTimeout)
                {
                    session.Dispose();
                }
                if (session.IsDisposed)
                {
                    DebugSession deleted;
                    if (_sessions.TryRemove(session.Id, out deleted))
                    {
                        Logger.Debug("Unused DebugSession [{0}] was removed", deleted.Id);
                    }
                }
            }

            _unusedSessionCollectorTimer.Enabled = !IsDisposed;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            IsDisposed = true;
            
            foreach (var session in _sessions.Values)
            {
                DebugSession deleted;
                if (_sessions.TryRemove(session.Id, out deleted))
                {
                    deleted.Dispose();
                }
            }
        }
    }
}
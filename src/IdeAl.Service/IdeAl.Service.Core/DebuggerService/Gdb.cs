﻿using System.IO;
using IdeAl.Communication.Common;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;
using IdeAl.Service.Core.Common;
using IdeAl.Service.Core.DebuggerService.Parsing;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace IdeAl.Service.Core.DebuggerService
{
    public partial class Gdb : IDebugger, IDisposableEx
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IGdbOutputParser _parser = new GdbOutputVisitor();
        private readonly Process _process;

        private const long IgnoredToken = 0L;
        private readonly IdGenerator _commandTokenGenerator = new IdGenerator(IgnoredToken);
        private readonly ManualResetEventSlim _commandPermission = new ManualResetEventSlim(true);
        private readonly BlockingCollection<string> _commandQueue = new BlockingCollection<string>();
        private readonly CancellationTokenSource _cancellation = new CancellationTokenSource();
        private readonly ConcurrentDictionary<long, Action<ResultRecord>> _manualResultHandlers = new ConcurrentDictionary<long, Action<ResultRecord>>();
        private readonly ConcurrentQueue<GdbEvent> _eventsQueue = new ConcurrentQueue<GdbEvent>();

        private readonly Dictionary<string, RegisterGroup> _registers = new Dictionary<string, RegisterGroup>();

        public Gdb(string exePath)
        {
            var oldErrorMode = Win32Native.SetErrorMode(ErrorModes.SEM_NOGPFAULTERRORBOX);
            
            var info = new ProcessStartInfo("gdb")
                       {
                           Arguments = CreateArgumentsString(exePath, "mi2"),
                           UseShellExecute = false,
                           ErrorDialog = false,
                           CreateNoWindow = true,
                           RedirectStandardInput = true,
                           RedirectStandardOutput = true,
                           RedirectStandardError = true,
                           StandardOutputEncoding = Encoding.GetEncoding(1251),
                           StandardErrorEncoding = Encoding.GetEncoding(1251)
                       };

            _process = new Process {StartInfo = info, EnableRaisingEvents = true};
            _process.OutputDataReceived += (sender, args) => ProcessOutput(args.Data);
            _process.ErrorDataReceived += (sender, args) => ProcessOutput(args.Data);

            _process.Start();
            Win32Native.SetErrorMode(oldErrorMode);

            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();

            Initialize(Path.GetDirectoryName(exePath));
        }

        private string CreateArgumentsString(string exePath, string interpreter)
        {
            const string quietArg = " -q";
            var interpreterArg = " -i=" + interpreter;
            return exePath + quietArg + interpreterArg;
        }

        private void Initialize(string workingDirectory)
        {
            RunCommandAsync(IgnoredToken, "-gdb-set charset CP1251");
            RunCommandAsync(IgnoredToken, "-gdb-set print sevenbit-strings off");
            RunCommandAsync(IgnoredToken, string.Format("-environment-cd \"{0}\"", workingDirectory.Replace(@"\", @"\\")));

            RunCommand("-data-list-register-names", SaveRegistersInfo);
            new Action(CommandRunningLoop).BeginInvoke(null, null);
        }

        public bool IsDisposed { get; private set; }

        public event Action Disposed = () => { };

        public void Dispose()
        {
            IsDisposed = true;
            _cancellation.Cancel();
            _process.TerminateTree();
            Disposed();
        }
        
        #region RunCommand - for internal usage

        private void RunCommand(string command, Action<ResultRecord> manualHandler)
        {
            using (var syncEvent = new AutoResetEvent(false))
            {
                // ReSharper disable once AccessToDisposedClosure (WaitOne)
                RunCommandAsync(command, record =>
                {
                    manualHandler(record);
                    syncEvent.Set();
                });
                syncEvent.WaitOne();
            }
        }

        private void RunCommandAsync(string command, Action<ResultRecord> resultHandler)
        {
            var token = _commandTokenGenerator.Next();
            _manualResultHandlers[token] = resultHandler;
            RunCommandAsync(token, command);
        }

        private void RunCommandAsync(long token, string command)
        {
            RunCommandCore(token + command);
        }

        private void RunCommandCore(string command)
        {
            Logger.Debug(command);
            _process.StandardInput.WriteLine(command);
        }

        #endregion

        #region EnqueueCommand - for normal usage

        private void EnqueueCommand(string command, Action<ResultRecord> resultHandler)
        {
            var token = _commandTokenGenerator.Next();
            _manualResultHandlers[token] = resultHandler;
            EnqueueCommand(token, command);
        }

        private void EnqueueCommand(string command)
        {
            EnqueueCommand(_commandTokenGenerator.Next(), command);
        }

        private void EnqueueCommand(long token, string command)
        {
            EnqueueCommandCore(token + command);
        }

        private void EnqueueCommandCore(string command)
        {
            _commandQueue.Add(command);
        }

        private void PermitCommandExecuting()
        {
            Logger.Debug("Permitted next command");
            _commandPermission.Set();
        }

        private bool WaitNextCommand(out string command)
        {
            try
            {
                Logger.Debug("Waiting for command permission");
                _commandPermission.Wait(_cancellation.Token);
                _commandPermission.Reset();

                Logger.Debug("Waiting for next command");
                command = _commandQueue.Take(_cancellation.Token);

                Logger.Debug("Next command: " + command);
                return true;
            }
            catch (OperationCanceledException)
            {
                command = null;
                return false;
            }
        }

        private void CommandRunningLoop()
        {
            string command;
            while (WaitNextCommand(out command))
            {
                RunCommandCore(command);
            }
        }

        #endregion

        #region Gdb Events processing

        public bool HasEvents { get { return !_eventsQueue.IsEmpty; } }

        public bool TryGetEvent(out GdbEvent gdbEvent)
        {
            return _eventsQueue.TryDequeue(out gdbEvent);
        }

        private void ProcessOutput(string data)
        {
            if (string.IsNullOrEmpty(data)) return;
            
            Logger.Debug(data);
            var gdbEvent = _parser.Parse(data);

            if (IsUnsupported(data, gdbEvent)) return;
            if (gdbEvent.Type == GdbEventType.Terminator || gdbEvent.Type == GdbEventType.StreamRecord) return;

            var resultRecord = (ResultRecord)gdbEvent;
            if (IsIgnored(resultRecord)) return;
            if (TryHandleManually(resultRecord)) return;
            if (TryHandleAutomatically(resultRecord)) return;

            Logger.Debug("Result {0} is not handled", resultRecord);
            AddEvent(resultRecord);
        }

        private bool IsUnsupported(string original, GdbEvent gdbEvent)
        {
            if (gdbEvent != null) return false;
            
            Logger.Warn("Event '{0}' cannot be parsed", original);
#warning Что это?
            PermitCommandExecuting();
            AddEvent(new ResultRecord("unsupported", new Dictionary<string, GdbValue> {{"msg", new GdbValue(original)}}));
            return true;
        }

        private bool IsIgnored(ResultRecord resultRecord)
        {
            if (!resultRecord.Token.HasValue) return false;
            if (resultRecord.Token.Value != 0L) return false;

            Logger.Debug("Result {0} is ignored", resultRecord);
            return true;
        }

        private bool TryHandleManually(ResultRecord resultRecord)
        {
            if (!resultRecord.Token.HasValue) return false;

            Action<ResultRecord> handler;
            if (!_manualResultHandlers.TryRemove(resultRecord.Token.Value, out handler)) return false;

            Logger.Debug("Result {0} is handled manually", resultRecord);
            handler(resultRecord);
            return true;
        }

        private bool TryHandleAutomatically(ResultRecord resultRecord)
        {
            if (resultRecord.Class != "stopped") return false;

            Logger.Debug("Result {0} is handled automatically", resultRecord);
            var reason = resultRecord.Result["reason"].Value<string>();
            switch (reason)
            {
                case "breakpoint-hit":
                case "end-stepping-range":
                    UpdateWatched();
                    PermitCommandExecuting();
                    AddEvent(resultRecord);
                    return true;
                case "exited":
                case "exited-normally":
                case "signal-received":
                    AddEvent(resultRecord);
                    Dispose();
                    return true;
                default:
                    Logger.Warn("ResultEvent 'stopped' with unknown reason '{0}'", reason);
                    AddEvent(resultRecord);
                    Dispose();
                    return true;
            }
        }

        private void AddEvent(GdbEvent gdbEvent)
        {
            _eventsQueue.Enqueue(gdbEvent);
        }

        #endregion
    }
}
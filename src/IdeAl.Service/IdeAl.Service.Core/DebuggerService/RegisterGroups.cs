﻿using System.Collections.Generic;
using IdeAl.Communication.Common;
using IdeAl.FrameworkExtensions;
using NLog;

namespace IdeAl.Service.Core.DebuggerService
{
    public static class Registers
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly Dictionary<string, RegisterGroup> Groups = new Dictionary<string, RegisterGroup>
                                                                           {
                                                                               {"rax", RegisterGroup.Cpu64},
                                                                               {"rbx", RegisterGroup.Cpu64},
                                                                               {"rcx", RegisterGroup.Cpu64},
                                                                               {"rdx", RegisterGroup.Cpu64},
                                                                               {"rsi", RegisterGroup.Cpu64},
                                                                               {"rdi", RegisterGroup.Cpu64},
                                                                               {"rbp", RegisterGroup.Cpu64},
                                                                               {"rsp", RegisterGroup.Cpu64},
                                                                               {"r8", RegisterGroup.Cpu64},
                                                                               {"r9", RegisterGroup.Cpu64},
                                                                               {"r10", RegisterGroup.Cpu64},
                                                                               {"r11", RegisterGroup.Cpu64},
                                                                               {"r12", RegisterGroup.Cpu64},
                                                                               {"r13", RegisterGroup.Cpu64},
                                                                               {"r14", RegisterGroup.Cpu64},
                                                                               {"r15", RegisterGroup.Cpu64},

                                                                               {"eax", RegisterGroup.Cpu32},
                                                                               {"ebx", RegisterGroup.Cpu32},
                                                                               {"ecx", RegisterGroup.Cpu32},
                                                                               {"edx", RegisterGroup.Cpu32},
                                                                               {"esi", RegisterGroup.Cpu32},
                                                                               {"edi", RegisterGroup.Cpu32},
                                                                               {"ebp", RegisterGroup.Cpu32},
                                                                               {"esp", RegisterGroup.Cpu32},
                                                                               {"r8d", RegisterGroup.Cpu32},
                                                                               {"r9d", RegisterGroup.Cpu32},
                                                                               {"r10d", RegisterGroup.Cpu32},
                                                                               {"r11d", RegisterGroup.Cpu32},
                                                                               {"r12d", RegisterGroup.Cpu32},
                                                                               {"r13d", RegisterGroup.Cpu32},
                                                                               {"r14d", RegisterGroup.Cpu32},
                                                                               {"r15d", RegisterGroup.Cpu32},

                                                                               {"ax", RegisterGroup.Cpu16},
                                                                               {"bx", RegisterGroup.Cpu16},
                                                                               {"cx", RegisterGroup.Cpu16},
                                                                               {"dx", RegisterGroup.Cpu16},
                                                                               {"si", RegisterGroup.Cpu16},
                                                                               {"di", RegisterGroup.Cpu16},
                                                                               {"bp", RegisterGroup.Cpu16},
                                                                               {"r8w", RegisterGroup.Cpu16},
                                                                               {"r9w", RegisterGroup.Cpu16},
                                                                               {"r10w", RegisterGroup.Cpu16},
                                                                               {"r11w", RegisterGroup.Cpu16},
                                                                               {"r12w", RegisterGroup.Cpu16},
                                                                               {"r13w", RegisterGroup.Cpu16},
                                                                               {"r14w", RegisterGroup.Cpu16},
                                                                               {"r15w", RegisterGroup.Cpu16},

                                                                               {"al", RegisterGroup.Cpu8},
                                                                               {"bl", RegisterGroup.Cpu8},
                                                                               {"cl", RegisterGroup.Cpu8},
                                                                               {"dl", RegisterGroup.Cpu8},
                                                                               {"sil", RegisterGroup.Cpu8},
                                                                               {"dil", RegisterGroup.Cpu8},
                                                                               {"bpl", RegisterGroup.Cpu8},
                                                                               {"spl", RegisterGroup.Cpu8},
                                                                               {"r8l", RegisterGroup.Cpu8},
                                                                               {"r9l", RegisterGroup.Cpu8},
                                                                               {"r10l", RegisterGroup.Cpu8},
                                                                               {"r11l", RegisterGroup.Cpu8},
                                                                               {"r12l", RegisterGroup.Cpu8},
                                                                               {"r13l", RegisterGroup.Cpu8},
                                                                               {"r14l", RegisterGroup.Cpu8},
                                                                               {"r15l", RegisterGroup.Cpu8},
                                                                               {"ah", RegisterGroup.Cpu8},
                                                                               {"bh", RegisterGroup.Cpu8},
                                                                               {"ch", RegisterGroup.Cpu8},
                                                                               {"dh", RegisterGroup.Cpu8},

                                                                               {"rip", RegisterGroup.CpuCtrl},
                                                                               {"eflags", RegisterGroup.CpuCtrl},
                                                                               {"cs", RegisterGroup.CpuCtrl},
                                                                               {"ss", RegisterGroup.CpuCtrl},
                                                                               {"ds", RegisterGroup.CpuCtrl},
                                                                               {"es", RegisterGroup.CpuCtrl},
                                                                               {"fs", RegisterGroup.CpuCtrl},
                                                                               {"gs", RegisterGroup.CpuCtrl},

                                                                               {"st0", RegisterGroup.Fpu80},
                                                                               {"st1", RegisterGroup.Fpu80},
                                                                               {"st2", RegisterGroup.Fpu80},
                                                                               {"st3", RegisterGroup.Fpu80},
                                                                               {"st4", RegisterGroup.Fpu80},
                                                                               {"st5", RegisterGroup.Fpu80},
                                                                               {"st6", RegisterGroup.Fpu80},
                                                                               {"st7", RegisterGroup.Fpu80},

                                                                               {"fctrl", RegisterGroup.FpuCtrl},
                                                                               {"fstat", RegisterGroup.FpuCtrl},
                                                                               {"ftag", RegisterGroup.FpuCtrl},
                                                                               {"fiseg", RegisterGroup.FpuCtrl},
                                                                               {"fioff", RegisterGroup.FpuCtrl},
                                                                               {"foseg", RegisterGroup.FpuCtrl},
                                                                               {"fooff", RegisterGroup.FpuCtrl},
                                                                               {"fop", RegisterGroup.FpuCtrl},

                                                                               {"xmm0", RegisterGroup.Sse128},
                                                                               {"xmm1", RegisterGroup.Sse128},
                                                                               {"xmm2", RegisterGroup.Sse128},
                                                                               {"xmm3", RegisterGroup.Sse128},
                                                                               {"xmm4", RegisterGroup.Sse128},
                                                                               {"xmm5", RegisterGroup.Sse128},
                                                                               {"xmm6", RegisterGroup.Sse128},
                                                                               {"xmm7", RegisterGroup.Sse128},
                                                                               {"xmm8", RegisterGroup.Sse128},
                                                                               {"xmm9", RegisterGroup.Sse128},
                                                                               {"xmm10", RegisterGroup.Sse128},
                                                                               {"xmm11", RegisterGroup.Sse128},
                                                                               {"xmm12", RegisterGroup.Sse128},
                                                                               {"xmm13", RegisterGroup.Sse128},
                                                                               {"xmm14", RegisterGroup.Sse128},
                                                                               {"xmm15", RegisterGroup.Sse128},

                                                                               {"mxcsr", RegisterGroup.SseCtrl},
                                                                           };

        public static RegisterGroup DetermineGroup(string registerName)
        {
            registerName.ThrowIfNull("registerName");

            RegisterGroup result;
            if (!Groups.TryGetValue(registerName, out result))
                Logger.Warn("Register '{0}' is determined as {1}", registerName, result);
            return result;
        }
    }
}
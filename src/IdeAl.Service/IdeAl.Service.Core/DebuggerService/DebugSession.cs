using System;
using System.Collections.Generic;
using IdeAl.Communication.Common;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;
using IdeAl.Service.Core.Common;

namespace IdeAl.Service.Core.DebuggerService
{
    public class DebugSession : IDisposableEx
    {
        private static readonly IdGenerator IdGenerator = new IdGenerator();
        public readonly long Id = IdGenerator.Next();

        private readonly FileLock _srcLock;
        private readonly FileLock _exeLock;

        private readonly Gdb _gdb;

        public DateTime LastActionTime = DateTime.Now;
        
        public DebugSession(string srcPath, string exePath)
        {
            srcPath.ThrowIfNullOrWhiteSpace("srcPath");
            exePath.ThrowIfNullOrWhiteSpace("exePath");

            _srcLock = new FileLock(srcPath);
            _exeLock = new FileLock(exePath);

            _gdb = new Gdb(exePath);
            _gdb.Disposed += Dispose;
        }

        public void Invoke(Action<IDebugger> action)
        {
            this.ThrowIfDisposed();
            action.ThrowIfNull("action");

            LastActionTime = DateTime.Now;
            action(_gdb);
        }

        public bool HasEvents { get { return _gdb.HasEvents; } }

        public GdbEvent[] GetEvents()
        {
            var result = new List<GdbEvent>();
            GdbEvent gdbEvent;
            while (_gdb.TryGetEvent(out gdbEvent))
                result.Add(gdbEvent);
            return result.ToArray();
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            IsDisposed = true;
            _gdb.Disposed -= Dispose;
            _gdb.Dispose();
            _srcLock.Dispose();
            _exeLock.Dispose();
        }
    }
}
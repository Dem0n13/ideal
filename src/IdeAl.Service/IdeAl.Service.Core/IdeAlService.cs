﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using IdeAl.Communication;
using IdeAl.Communication.Input;
using IdeAl.Communication.Output;
using IdeAl.Communication.Output.Gdb;

namespace IdeAl.Service.Core
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class IdeAlService : IIdeAlService
    {
        private readonly FileService.FileService _fileService = new FileService.FileService();
        private readonly CompilerService.CompilerService _compilerService = new CompilerService.CompilerService();
        private readonly DebuggerService.DebuggerService _debuggerService = new DebuggerService.DebuggerService();

        public BaseServiceResult GetEvents()
        {
            return Try(() =>
                       {
                           var result = _debuggerService.GetEvents();
                           return new ValueServiceResult<Dictionary<long, GdbEvent[]>>(result);
                       });
        }

        public BaseServiceResult GetFileList(string directory)
        {
            return Try(() =>
                       {
                           var result = _fileService.GetFileList(directory);
                           return new ValueServiceResult<string[]>(result);
                       });
        }

        public BaseServiceResult ReadFileContent(string directory, string fileName)
        {
            return Try(() =>
                       {
                           var content = _fileService.ReadFileContent(directory, fileName);
                           return new ValueServiceResult<string>(content);
                       });
        }

        public BaseServiceResult DownloadFileContent(string directory, string fileName)
        {
            return Try(() =>
                       {
                           var content = _fileService.DownloadFileContent(directory, fileName);
                           return new ValueServiceResult<byte[]>(content);
                       });
        }

        public BaseServiceResult WriteFileContent(string directory, string fileName, string content)
        {
            return Try(() =>
                       {
                           _fileService.WriteFileContent(directory, fileName, content);
                           return new CompletedServiceResult();
                       });
        }

        public BaseServiceResult DeleteFile(string directory, string fileName)
        {
            return Try(() =>
                       {
                           _fileService.DeleteFile(directory, fileName);
                           return new CompletedServiceResult();
                       });
        }

        public BaseServiceResult Compile(string directory, string fileName)
        {
            return Try(() =>
                       {
                           var result = _compilerService.Compile(directory, fileName, true);
                           return new CompilerServiceResult(result.Compiled, result.ErrorList);
                       });
        }

        public BaseServiceResult StartDebugSession(string directory, string srcFileName)
        {
            return Try(() =>
                       {
                           var sessionId = _debuggerService.StartSession(directory, srcFileName);
                           return new StartedServiceResult(sessionId);
                       });
        }

        public BaseServiceResult StopDebugSession(long sessionId)
        {
            return Try(() =>
                       {
                           _debuggerService.StopSession(sessionId);
                           return new CompletedServiceResult();
                       });
        }

        public BaseServiceResult ExecuteDebugCommand(long sessionId, BaseDebugCommand command)
        {
            return Try(() =>
                       {
                           _debuggerService.Execute(sessionId, command.Run);
                           return new StartedServiceResult(sessionId);
                       });
        }

        public BaseServiceResult NotImplementedOperation()
        {
            return new ExceptionServiceResult(new NotImplementedException("IdeAlService.NotImplementedOperation"));
        }

        private BaseServiceResult Try(Func<BaseServiceResult> action)
        {
            try
            {
                return action();
            }
            catch (Exception exception)
            {
                return new ExceptionServiceResult(exception);
            }
        }
    }
}
using System.IO;
using System.Linq;
using System.Text;
using IdeAl.FrameworkExtensions;

namespace IdeAl.Service.Core.FileService
{
    public class FileService
    {
        private static readonly Encoding Win1251 = Encoding.GetEncoding(1251);

        public string[] GetFileList(string directory)
        {
            directory.ThrowIfNullOrWhiteSpace("directory");

            return (Directory.Exists(directory))
                ? Directory.GetFiles(directory).Select(Path.GetFileName).ToArray()
                : new string[0];
        }

        public void WriteFileContent(string directory, string fileName, string content)
        {
            directory.ThrowIfNull("directory");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            CreateDirectoryIfNotExists(directory);
            fileName = Path.GetFileName(fileName);
            File.WriteAllText(Path.Combine(directory, fileName), content, Win1251);
        }

        public string ReadFileContent(string directory, string fileName)
        {
            directory.ThrowIfNull("directory");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            CreateDirectoryIfNotExists(directory);
            fileName = Path.GetFileName(fileName);
            return File.ReadAllText(Path.Combine(directory, fileName), Win1251);
        }

        public byte[] DownloadFileContent(string directory, string fileName)
        {
            directory.ThrowIfNull("directory");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            CreateDirectoryIfNotExists(directory);
            fileName = Path.GetFileName(fileName);
            return File.ReadAllBytes(Path.Combine(directory, fileName));
        }

        public void RenameFile(string directory, string oldFileName, string newFileName)
        {
            directory.ThrowIfNull("directory");
            oldFileName.ThrowIfNullOrWhiteSpace("oldFileName");
            newFileName.ThrowIfNullOrWhiteSpace("newFileName");

            CreateDirectoryIfNotExists(directory);
            oldFileName = Path.GetFileName(oldFileName);
            newFileName = Path.GetFileName(newFileName);
            File.Move(Path.Combine(directory, oldFileName), Path.Combine(directory, newFileName));
        }

        public void DeleteFile(string directory, string fileName)
        {
            directory.ThrowIfNull("directory");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            fileName = Path.GetFileName(fileName);
            var fullPath = Path.Combine(directory, fileName);
            if (File.Exists(fullPath))
                File.Delete(fullPath);
        }

        private void CreateDirectoryIfNotExists(string directory)
        {
            if (directory == string.Empty) return;
            Directory.CreateDirectory(directory);
        }
    }
}
﻿using System;
using System.IO;

namespace IdeAl.Service.Core.Common
{
    public class FileLock : IDisposable
    {
        private readonly FileStream _fileStream;

        public FileLock(string path)
        {
            _fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
        }

        public void Dispose()
        {
            _fileStream.Dispose();
        }
    }
}
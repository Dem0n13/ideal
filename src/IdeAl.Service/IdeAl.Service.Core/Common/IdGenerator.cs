﻿using System.Threading;

namespace IdeAl.Service.Core.Common
{
    public class IdGenerator
    {
        private long _currentId;

        public IdGenerator() : this(default(long))
        {
        }

        public IdGenerator(long seed)
        {
            _currentId = seed;
        }

        public long Next()
        {
            return Interlocked.Increment(ref _currentId);
        }
    }
}
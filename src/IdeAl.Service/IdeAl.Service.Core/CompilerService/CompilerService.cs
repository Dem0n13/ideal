﻿using System.IO;
using IdeAl.FrameworkExtensions;
using IdeAl.Service.Core.Common;

namespace IdeAl.Service.Core.CompilerService
{
    public class CompilerService
    {
        public CompilerResult Compile(string directory, string srcFileName, bool debug)
        {
            directory.ThrowIfNull("directory");
            srcFileName.ThrowIfNullOrWhiteSpace("srcFileName");

            srcFileName = Path.GetFileName(srcFileName);
            var srcPath = Path.Combine(directory, srcFileName);
            var exePath = Path.ChangeExtension(srcPath, "exe");

            using (new FileLock(srcPath))
            {
                using (var gcc = new Gcc(srcPath, exePath))
                {
                    return gcc.Compile(debug);
                }
            }
        }
    }
}
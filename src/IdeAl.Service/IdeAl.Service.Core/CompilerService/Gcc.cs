﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using IdeAl.FrameworkExtensions;

namespace IdeAl.Service.Core.CompilerService
{
    public class Gcc : ICompiler
    {
        private readonly string _srcPath;
        private readonly string _exePath;
        private readonly Process _process;

        public Gcc(string srcPath, string exePath)
        {
            srcPath.ThrowIfNull("srcPath");
            srcPath.ThrowIfNull("exePath");

            _srcPath = srcPath;
            _exePath = exePath;
            var info = new ProcessStartInfo("gcc")
            {
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            _process = new Process {StartInfo = info};
        }

        public CompilerResult Compile(bool debug)
        {
            var outArg = " -o " + _exePath;
            var debugArg = debug ? " -g" : string.Empty;

            _process.StartInfo.Arguments = _srcPath + outArg + debugArg;
            _process.Start();
            _process.WaitForExit();


            var lastErrors = new List<string>();
            while (!_process.StandardError.EndOfStream)
            {
                lastErrors.Add(_process.StandardError.ReadLine());
            }

            return new CompilerResult(lastErrors.Count == 0, lastErrors);
        }

        void IDisposable.Dispose()
        {
            _process.Dispose();
        }
    }
}
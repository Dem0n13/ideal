﻿using System;

namespace IdeAl.Service.Core.CompilerService
{
    public interface ICompiler : IDisposable
    {
        CompilerResult Compile(bool debug);
    }
}
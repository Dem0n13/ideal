﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IdeAl.Service.Core.CompilerService
{
    [DataContract]
    public class CompilerResult
    {
        [DataMember] public readonly bool Compiled;
        [DataMember] public readonly string[] ErrorList;

        public CompilerResult(bool compiled, IEnumerable<string> lastErrors)
        {
            Compiled = compiled;
            ErrorList = lastErrors.ToArray();
        }
    }
}
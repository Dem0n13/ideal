﻿using System;
using System.ServiceModel;
using System.Threading;
using IdeAl.Communication;
using IdeAl.Service.Core;
using NLog;

namespace IdeAl.Service.ConsoleApplication
{
    internal partial class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main()
        {
            ConfigureNLog();
            
            using (var host = new ServiceHost(typeof (IdeAlService), new[] {new Uri("net.pipe://localhost")}))
            {
                using (var serviceStartedEvent = new EventWaitHandle(false, EventResetMode.ManualReset, "IdeAlService"))
                {
                    host.AddServiceEndpoint(typeof (IIdeAlService), new NetNamedPipeBinding(), "IdeAlService");
                    host.Open();

                    Logger.Info("Service is available. Press <ENTER> to exit.");
                    serviceStartedEvent.Set();
                    Console.ReadLine();
                }
            }
        }
    }
}

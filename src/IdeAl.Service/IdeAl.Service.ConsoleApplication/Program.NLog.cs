﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace IdeAl.Service.ConsoleApplication
{
    internal partial class Program
    {
        public static void ConfigureNLog()
        {
            var config = new LoggingConfiguration();

            // layouts
            const string shortLayout = "${time}|${level:uppercase=true}|${logger:shortName=true}|${message} ${exception:format=tostring}";
            const string longLayout = "${longdate}|${level:uppercase=true}|${message} ${exception:format=tostring}";

            // targets
            var debugger = new DebuggerTarget { Layout = longLayout };
            var console = new ColoredConsoleTarget {Layout = shortLayout};
            var tempFile = new FileTarget {DeleteOldFileOnStartup = true, FileName = "log.txt", Layout = shortLayout};

            // rules
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, debugger));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, console));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, tempFile)); 

            LogManager.Configuration = config;
        }
    }
}

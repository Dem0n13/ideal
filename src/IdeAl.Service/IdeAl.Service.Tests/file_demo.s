    .intel_syntax noprefix

    .data
file_mode:
    .asciz "w+"
file_name:
    .asciz "file.hex"
file:
    .quad 0
    
data:
    .int 0xBEEFBEEF
    .int 0xAAAABBEE
data_length:
    .int ($-data) / 4
    
    .text
    .global main
main:
    push rbp
    mov rbp, rsp
    sub rsp, 48

    lea rdx, file_mode
    lea rcx, file_name
    call fopen
    mov file, rax
    
    mov rdx, file
    mov r9, rdx
    mov r8d, data_length    # array length
    mov edx, 4              # size of each element
    lea rcx, data           # data pointer
    call fwrite
    
    mov rcx, file
    call fclose
    
    mov eax, 0
    add rsp, 48
    pop rbp
    ret

﻿using System.Collections.Generic;
using IdeAl.Communication.Output.Gdb;
using IdeAl.Service.Core.DebuggerService.Parsing;
using NUnit.Framework;

namespace IdeAl.Service.Tests
{
    [TestFixture]
    public class LexerTests
    {
        private readonly IGdbOutputParser _parser = new GdbOutputVisitor();

        [Test]
        public void StreamRecord()
        {
            const string input = "~\"Reading symbols from C:\\\\src.exe...\"";

            var output = (StreamRecord) _parser.Parse(input);
            Assert.AreEqual("Reading symbols from C:\\src.exe...", output.Message);
        }

        [Test]
        public void DoneBreakpointRecord()
        {
            const string input = "^done,bkpt={number=\"1\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"0x00000000004014e4\",file=\"src.s\",fullname=\"C:\\\\src.s\",line=\"17\",thread-groups=[\"i1\"],times=\"0\",original-location=\"main\"}";

            var output = (ResultRecord) _parser.Parse(input);
            Assert.AreEqual(null, output.Token);
            Assert.AreEqual("done", output.Class);

            var result = output.Result;
            Assert.AreEqual(1, result.Count);
            Assert.IsTrue(result.ContainsKey("bkpt"));

            var parameters = result["bkpt"].Value<Dictionary<string, GdbValue>>();
            Assert.AreEqual("1", parameters["number"].Value<string>());
            Assert.AreEqual("breakpoint", parameters["type"].Value<string>());
            Assert.AreEqual("keep", parameters["disp"].Value<string>());
            Assert.AreEqual("y", parameters["enabled"].Value<string>());
            Assert.AreEqual("0x00000000004014e4", parameters["addr"].Value<string>());
            Assert.AreEqual("src.s", parameters["file"].Value<string>());
            Assert.AreEqual("C:\\src.s", parameters["fullname"].Value<string>());
            Assert.AreEqual("17", parameters["line"].Value<string>());
            CollectionAssert.IsNotEmpty(parameters["thread-groups"].Value<GdbValue[]>());
            Assert.AreEqual("0", parameters["times"].Value<string>());
            Assert.AreEqual("main", parameters["original-location"].Value<string>());
        }

        [Test]
        public void StoppedBreakpointRecord()
        {
            const string input = "*stopped,reason=\"breakpoint-hit\",disp=\"keep\",bkptno=\"1\",frame={addr=\"0x00000000004014e4\",func=\"main\",args=[],file=\"src.s\",fullname=\"C:\\\\src.s\",line=\"17\"},thread-id=\"1\",stopped-threads=\"all\"";

            var output = (ResultRecord)_parser.Parse(input);
            Assert.AreEqual(null, output.Token);
            Assert.AreEqual("stopped", output.Class);

            var result = output.Result;
            Assert.AreEqual(6, result.Count);
            Assert.AreEqual("breakpoint-hit", result["reason"].Value<string>());
            Assert.AreEqual("keep", result["disp"].Value<string>());
            Assert.AreEqual("1", result["bkptno"].Value<string>());
            Assert.AreEqual("1", result["thread-id"].Value<string>());
            Assert.AreEqual("all", result["stopped-threads"].Value<string>());

            var frame = result["frame"].Value<Dictionary<string, GdbValue>>();
            Assert.AreEqual("0x00000000004014e4", frame["addr"].Value<string>());
            Assert.AreEqual("main", frame["func"].Value<string>());
            CollectionAssert.IsEmpty(frame["args"].Value<GdbValue[]>());
            Assert.AreEqual("src.s", frame["file"].Value<string>());
            Assert.AreEqual("C:\\src.s", frame["fullname"].Value<string>());
            Assert.AreEqual("17", frame["line"].Value<string>());
        }
        
        [Test]
        public void TerminatorRecord()
        {
            const string input = "(gdb)";

            var output = (TerminatorRecord) _parser.Parse(input);
            Assert.IsNotNull(output);
        }

        [Test]
        public void CustomResultClass()
        {
            const string input = "=thread-group-started,id=\"i1\",pid=\"6080\"";

            var output = _parser.Parse(input);
            Assert.AreEqual(GdbEventType.ResultRecord, output.Type);

            var resultRecord = (ResultRecord) output;
            Assert.AreEqual("thread-group-started", resultRecord.Class);
        }

        [Test]
        public void BreakInCStrings()
        {
            const string input = "~\"done.\\n\"";

            var output = _parser.Parse(input);
            Assert.AreEqual(GdbEventType.StreamRecord, output.Type);

            var streamRecord = (StreamRecord) output;
            Assert.AreEqual("done.", streamRecord.Message);
        }

        [Test]
        public void VarCreation()
        {
            const string input = "17^done,name=\"r15\",numchild=\"0\",value=\"\",type=\"int64_t\",has_more=\"0\"";

            var output = _parser.Parse(input);
            Assert.AreEqual(GdbEventType.ResultRecord, output.Type);

            var resultRecord = (ResultRecord)output;
            Assert.AreEqual("done", resultRecord.Class);

            Assert.AreEqual("r15", resultRecord.Result["name"].Value<string>());
            Assert.AreEqual("int64_t", resultRecord.Result["type"].Value<string>());
            Assert.AreEqual("", resultRecord.Result["value"].Value<string>());
        }
    }
}
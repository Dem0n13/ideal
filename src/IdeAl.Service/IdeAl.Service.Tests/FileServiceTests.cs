﻿using System.IO;
using System.Text;
using IdeAl.Service.Core.FileService;
using NUnit.Framework;

namespace IdeAl.Service.Tests
{
    public class FileServiceTests
    {
        private const string Directory = "test/test/test";
        private const string FileName = "test.tmp";
        private const string FileName2 = "test2.tmp";
        private const string Text = "Привет, мир!";
        private readonly string _path = Path.Combine(Directory, FileName);
        private readonly string _path2 = Path.Combine(Directory, FileName2);
        private readonly FileService _fileService = new FileService();

        [SetUp]
        public void SetUp()
        {
            if (File.Exists(_path)) File.Delete(_path);
            if (File.Exists(_path2)) File.Delete(_path2);
        }

        [Test]
        public void WriteRead()
        {
            _fileService.WriteFileContent(Directory, FileName, Text);
            var content = _fileService.ReadFileContent(Directory, FileName);

            Assert.AreEqual(Text, content);
        }

        [Test]
        public void Download()
        {
            var cp1251 = Encoding.GetEncoding(1251);
            var textBytes = cp1251.GetBytes(Text);

            _fileService.WriteFileContent(Directory, FileName, Text);
            var content = _fileService.DownloadFileContent(Directory, FileName);
            CollectionAssert.AreEqual(textBytes, content);
        }

        [Test]
        public void Renaming()
        {
            _fileService.WriteFileContent(Directory, FileName, Text);
            _fileService.RenameFile(Directory, FileName, FileName2);
            _fileService.RenameFile(Directory, FileName2, "ff/fff/" + FileName);
            var content = _fileService.ReadFileContent(Directory, FileName);

            Assert.AreEqual(Text, content);
        }

        [Test]
        public void Deleting()
        {
            _fileService.WriteFileContent(Directory, FileName, Text);
            _fileService.DeleteFile(Directory, "../../" + FileName);

            CollectionAssert.IsEmpty(_fileService.GetFileList(Directory));
        }
    }
}
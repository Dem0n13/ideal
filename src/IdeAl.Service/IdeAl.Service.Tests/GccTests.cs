﻿using System.Diagnostics;
using System.IO;
using IdeAl.Service.Core.CompilerService;
using NUnit.Framework;

namespace IdeAl.Service.Tests
{
    [TestFixture]
    public class GccTests
    {
        [SetUp]
        public void SetUp()
        {
            if (File.Exists("demo.exe"))
                File.Delete("demo.exe");
            if (File.Exists("demo1.exe"))
                File.Delete("demo1.exe");
        }

        [Test]
        public void Compile()
        {
            using (var gcc = new Gcc("demo.s", "demo.exe"))
            {
                var result = gcc.Compile(true);

                Assert.IsTrue(result.Compiled);
                Assert.IsTrue(File.Exists("demo.exe"));
            }

            using (var gcc = new Gcc("demo.s", "demo1.exe"))
            {
                var result = gcc.Compile(true);

                Assert.IsTrue(result.Compiled);
                Assert.IsTrue(File.Exists("demo1.exe"));
            }
        }

        [Test]
        public void CompileWithError()
        {
            using (var gcc = new Gcc("demo_with_errors.s", "demo.exe"))
            {
                var result = gcc.Compile(true);

                Assert.IsFalse(result.Compiled);
                CollectionAssert.IsNotEmpty(result.ErrorList);
                Assert.IsFalse(File.Exists("demo.exe"));

                foreach (var error in result.ErrorList)
                    Debug.WriteLine(error);
            }
        }
    }
}
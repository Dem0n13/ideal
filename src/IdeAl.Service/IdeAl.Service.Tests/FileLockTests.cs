﻿using System.IO;
using IdeAl.Service.Core.Common;
using NUnit.Framework;

namespace IdeAl.Service.Tests
{
    [TestFixture]
    public class FileLockTests
    {
        private const string FileName = "test.tmp";
        private const string Text = "Text";
        
        [SetUp]
        public void SetUp()
        {
            File.WriteAllText(FileName, Text);
        }

        [Test]
        public void Usage()
        {
            using (new FileLock(FileName))
            {
                Assert.Throws<IOException>(() => File.WriteAllText(FileName, "New Text"));
                var content = File.ReadAllText(FileName);
                Assert.AreEqual(Text, content);
            }
        }

        [TearDown]
        public void TearDown()
        {
            File.Delete(FileName);
        }
    }
}
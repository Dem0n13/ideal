﻿using System;
using System.Threading;
using IdeAl.Service.Core.CompilerService;
using IdeAl.Service.Core.DebuggerService;
using NUnit.Framework;

namespace IdeAl.Service.Tests
{
    [TestFixture]
    public class GdbTests
    {
        private const string DemoSrcPath = "demo.s";
        private const string DemoExePath = "demo.exe";

        private const string LongDemoSrcPath = "long_demo.s";
        private const string LongDemoExePath = "long_demo.exe";

        [SetUp]
        public void SetUp()
        {
            using (var gcc = new Gcc(LongDemoSrcPath, LongDemoExePath))
                gcc.Compile(true);
            using (var gcc = new Gcc(DemoSrcPath, DemoExePath))
                gcc.Compile(true);
        }

        [Test]
        public void StartAutoStopDemo()
        {
            var gdb = new Gdb(DemoExePath);

            gdb.Start();
            Thread.Sleep(1000);
            Assert.IsTrue(gdb.HasEvents);
            gdb.Dispose();
            gdb.Dispose();
        }

        [Test]
        public void StartStopLongDemo()
        {
            var gdb = new Gdb(LongDemoExePath);
            
            gdb.Start();
            Thread.Sleep(1000);
            Assert.IsTrue(gdb.HasEvents);
            gdb.Dispose();
        }
    }
}
﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace IdeAl.Service.WindowsService
{
    public partial class Program
    {
        public static void ConfigureNLog()
        {
            var config = new LoggingConfiguration();

            // targets
            var consoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", consoleTarget);
            var debugTarget = new DebuggerTarget();
            config.AddTarget("debug", debugTarget);

            // rules
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, debugTarget));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, consoleTarget));

            LogManager.Configuration = config;
        }
    }
}

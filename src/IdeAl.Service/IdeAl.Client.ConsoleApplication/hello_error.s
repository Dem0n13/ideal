	.intel_syntax noprefix
	.comm	data, 4, 2
	.section .rdata,"dr"
.LC0:
	.ascii "Hello\317\360\350\342\345\362\n\0"
	.text
	.global main
main:
	push	rbp
	mov	rbp, rsp
	sub	rsp, 32
	lea	rax, data[rip]
	mov	DWORD PTR [rax], 666
	lea	rcx, .LC0[rip]
	call	printf
	mov	eax, 0
	add	rsp, 32
	pop	rbp
	ret

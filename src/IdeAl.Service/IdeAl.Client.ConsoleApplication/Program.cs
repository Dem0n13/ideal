﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Threading;
using IdeAl.Communication;
using IdeAl.Communication.Common;
using IdeAl.Communication.Input;
using IdeAl.Communication.Output;
using IdeAl.Communication.Output.Gdb;

namespace IdeAl.Client.ConsoleApplication
{
    internal class Program
    {
        private static ServiceClient _client;

        private static void Main()
        {
            WaitForServiceStarted();

            var channelFactory = new ChannelFactory<IIdeAlService>(
                new NetNamedPipeBinding(),
                new EndpointAddress("net.pipe://localhost/IdeAlService"));

            _client = new ServiceClient(channelFactory);
            _client.ServiceEventsRecieved += ServiceEventRecieved;
            _client.StartReceivingServiceEvents();

            var source = File.ReadAllText("hello.s", Encoding.GetEncoding(1251));

            var result = _client.Invoke(service => service.WriteFileContent("test", "demo.s", source));
            Console.WriteLine("WriteFileContent: " + result.OperationResult);

            result = _client.Invoke(service => service.Compile("test", "demo.s"));
            Console.WriteLine("Compile: " + result.OperationResult);

            result = _client.Invoke(service => service.DownloadFileContent("test", "demo.exe"));
            Console.WriteLine("DownloadFileContent: " + result.OperationResult);

            result = _client.Invoke(service => service.GetFileList("test"));
            Console.WriteLine("DownloadFileContent: " + result.OperationResult);

            result = _client.Invoke(service => service.StartDebugSession("test", "demo.s"));
            Console.WriteLine("StartDebugSession: " + result.OperationResult);

            var sessionId = ((StartedServiceResult) result).SessionId;

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new WatchCommand(RegisterGroup.Cpu64)));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new AddBreakpointCommand("main")));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new StartCommand()));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new WatchCommand("str", VarType.String)));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new SetValueCommand("rax", "12345")));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new UnwatchCommand("rax")));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new UnwatchCommand(RegisterGroup.Cpu64)));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new StepIntoCommand()));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            result = _client.Invoke(service => service.ExecuteDebugCommand(sessionId, new ContinueCommand()));
            Console.WriteLine("ExecuteDebugCommand: " + result.OperationResult);

            Console.ReadLine();
        }

        private static void ServiceEventRecieved()
        {
            KeyValuePair<long, GdbEvent[]> eventPack;
            while (_client.ServiceEventQueue.TryDequeue(out eventPack))
            {
                ProcessServiceEventPack(eventPack);
            }
        }

        private static void ProcessServiceEventPack(KeyValuePair<long, GdbEvent[]> serviceEventPack)
        {
            foreach (var gdbEvent in serviceEventPack.Value)
            {
                Console.WriteLine("Для пользователя {0} получено событие: {1}", serviceEventPack.Key, gdbEvent);
            }
        }

        private static void WaitForServiceStarted()
        {
            var serviceStartedEvent = new EventWaitHandle(false, EventResetMode.ManualReset, "IdeAlService");
            serviceStartedEvent.WaitOne();
        }
    }
}

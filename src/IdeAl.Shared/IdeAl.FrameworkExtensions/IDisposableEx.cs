﻿using System;

namespace IdeAl.FrameworkExtensions
{
    public interface IDisposableEx : IDisposable
    {
        bool IsDisposed { get; }
    }
}
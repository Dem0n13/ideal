﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Management;
using NLog;

namespace IdeAl.FrameworkExtensions
{
    public static class ProcessExtensions
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void Terminate(this Process process)
        {
            process.ThrowIfNull("process");

            Logger.Debug("Process termination: [{0}]", process.Id);
            try
            {
                process.Kill();
            }
            catch (InvalidOperationException)
            {
                // already exited
            }
            catch (Exception ex)
            {
                Logger.DebugException("Process.Kill: ", ex);
            }
            finally
            {
                process.Close();
            }
        }

        public static void TerminateTree(this Process parentProcess)
        {
            parentProcess.ThrowIfNull("parentProcess");

            foreach (var processId in GetProcessTree(parentProcess.Id))
            {
                Process process;
                if (TryGetProcess(processId, out process))
                    process.Terminate();
            }
        }

        private static bool TryGetProcess(int id, out Process process)
        {
            try
            {
                process = Process.GetProcessById(id);
                return true;
            }
            catch (ArgumentException)
            {
                process = null;
                return false;
            }
        }

        private static IEnumerable<int> GetProcessTree(int parentProcessId)
        {
            var tree = new HashSet<int>();
            GetProcessTree(parentProcessId, tree);
            return tree;
        }

        private static void GetProcessTree(int parentProcessId, HashSet<int> accumulator)
        {
            accumulator.Add(parentProcessId);

            var searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + parentProcessId);

            var childrenIds = searcher.Get()
                .OfType<ManagementObject>()
                .Select(process => Convert.ToInt32(process["ProcessID"]))
                .Where(id => !accumulator.Contains(id));

            foreach (var childProcessId in childrenIds)
            {
                GetProcessTree(childProcessId, accumulator);
            }
        }
    }
}
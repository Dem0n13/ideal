﻿using System;
using System.Diagnostics;

namespace IdeAl.FrameworkExtensions
{
    public static class ThrowIfExtensions
    {
        [DebuggerHidden]
        public static void ThrowIfNull<T>(this T value, string paramName) where T : class
        {
            if (value == null)
                throw new ArgumentNullException(paramName);
        }

        [DebuggerHidden]
        public static void ThrowIfNull(this string value, string paramName)
        {
            if (value == null)
                throw new ArgumentNullException(paramName);
        }

        [DebuggerHidden]
        public static void ThrowIfNullOrWhiteSpace(this string value, string paramName)
        {
            if (value == null)
                throw new ArgumentNullException(paramName);
            if (value.Trim() == string.Empty)
                throw new ArgumentException("Argument cannot be empty", paramName);
        }

        [DebuggerHidden]
        public static void ThrowIfDisposed<T>(this T value) where T : IDisposableEx
        {
            if (value.IsDisposed)
                throw new ObjectDisposedException(typeof (T).FullName);
        }

        [DebuggerHidden]
        public static void ThrowIfUnknown<TEnum>(this TEnum value, string paramName) where TEnum : struct, IConvertible
        {
            if (!Enum.IsDefined(typeof(TEnum), value))
                throw new ArgumentException("Argument is not one of the named constants defined for the enumeration", paramName);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Reflection;
using IdeAl.Communication.Input;
using IdeAl.FrameworkExtensions;

namespace IdeAl.Communication
{
    public class IdeAlServiceHelper
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            return typeof(BaseDebugCommand).GetDerivedTypes();
        }
    }
}
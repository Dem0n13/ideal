using System.Runtime.Serialization;

namespace IdeAl.Communication.Output
{
    [DataContract]
    public class ValueServiceResult<T> : CompletedServiceResult
    {
        [DataMember] public readonly T Value;

        public ValueServiceResult(T value)
        {
            Value = value;
        }
    }
}
using System.Collections.Generic;
using System.Runtime.Serialization;
using IdeAl.Communication.Output.Gdb;

namespace IdeAl.Communication.Output
{
    [DataContract]
    [KnownType(typeof (CompilerServiceResult))]
    [KnownType(typeof (ValueServiceResult<string>))]
    [KnownType(typeof (ValueServiceResult<string[]>))]
    [KnownType(typeof (ValueServiceResult<byte[]>))]
    [KnownType(typeof (ValueServiceResult<Dictionary<long, GdbEvent[]>>))]
    public class CompletedServiceResult : BaseServiceResult
    {
        public CompletedServiceResult() : base(OperationResult.Completed)
        {
        }
    }
}
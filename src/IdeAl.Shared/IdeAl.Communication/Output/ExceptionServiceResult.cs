﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace IdeAl.Communication.Output
{
    [DataContract]
    public class ExceptionServiceResult : BaseServiceResult
    {
        [DataMember] public readonly ExceptionDetail Detail;

        public ExceptionServiceResult(Exception exception) : base(OperationResult.Failed)
        {
            Detail = new ExceptionDetail(exception);
        }
    }
}
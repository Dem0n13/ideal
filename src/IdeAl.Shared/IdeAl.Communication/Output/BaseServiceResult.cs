﻿using System;
using System.Runtime.Serialization;

namespace IdeAl.Communication.Output
{
    [DataContract]
    [KnownType(typeof (StartedServiceResult))]
    [KnownType(typeof (CompletedServiceResult))]
    [KnownType(typeof (ExceptionServiceResult))]
    public abstract class BaseServiceResult
    {
        [DataMember] public readonly OperationResult OperationResult;

        protected BaseServiceResult(OperationResult operationResult)
        {
            if (!Enum.IsDefined(typeof (OperationResult), operationResult))
                throw new ArgumentOutOfRangeException("operationResult");
            OperationResult = operationResult;
        }
    }
}
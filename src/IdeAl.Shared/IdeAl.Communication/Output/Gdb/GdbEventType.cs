﻿namespace IdeAl.Communication.Output.Gdb
{
    public enum GdbEventType
    {
        ResultRecord,
        StreamRecord,
        Terminator
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IdeAl.Communication.Output.Gdb
{
    [DataContract]
    [KnownType(typeof (Dictionary<string, GdbValue>))]
    [KnownType(typeof (GdbValue[]))]
    public class GdbValue
    {
        [DataMember] public readonly GdbValueType Type;
        [DataMember] private readonly object _value;

        public GdbValue(string @const)
        {
            Type = GdbValueType.Const;
            _value = @const;
        }

        public GdbValue(GdbValue[] list)
        {
            Type = GdbValueType.List;
            _value = list;
        }

        public GdbValue(Dictionary<string, GdbValue> tuple)
        {
            Type = GdbValueType.Tuple;
            _value = tuple;
        }

        public T Value<T>()
        {
            return (T) _value;
        }

        public override string ToString()
        {
            switch (Type)
            {
                case GdbValueType.Const:
                    return (string) _value;
                case GdbValueType.List:
                    var list = (GdbValue[]) _value;
                    return "Array[" + list.Length + "]";
                case GdbValueType.Tuple:
                    var tuple = (Dictionary<string, GdbValue>) _value;
                    return "Dictionary[" + tuple.Count + "]";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static GdbValue From(IEnumerable<string> list)
        {
            return new GdbValue(list.Select(value => new GdbValue(value)).ToArray());
        }
    }
}
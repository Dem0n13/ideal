﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IdeAl.Communication.Output.Gdb
{
    [DataContract]
    public class ResultRecord : GdbEvent
    {
        [DataMember] public readonly long? Token;
        [DataMember] public readonly string Class;
        [DataMember] public readonly Dictionary<string, GdbValue> Result;
        
        public ResultRecord(string @class, Dictionary<string, GdbValue> result) : this(null, @class, result)
        {
        }

        public ResultRecord(long? token, string @class, Dictionary<string, GdbValue> result) : base(GdbEventType.ResultRecord)
        {
            Token = token;
            Class = @class;
            Result = result;
        }

        public override string ToString()
        {
            return string.Format("[{0}]{1}", Token, Class);
        }
    }
}
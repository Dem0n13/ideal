﻿using System.Runtime.Serialization;

namespace IdeAl.Communication.Output.Gdb
{
    [DataContract]
    public class TerminatorRecord : GdbEvent
    {
        public TerminatorRecord() : base(GdbEventType.Terminator)
        {
        }

        public override string ToString()
        {
            return "[gdb]";
        }
    }
}
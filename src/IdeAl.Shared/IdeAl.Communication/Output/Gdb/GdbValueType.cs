namespace IdeAl.Communication.Output.Gdb
{
    public enum GdbValueType
    {
        Const,
        List,
        Tuple
    }
}
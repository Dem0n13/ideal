﻿using System.Runtime.Serialization;

namespace IdeAl.Communication.Output.Gdb
{
    [DataContract]
    [KnownType(typeof (ResultRecord))]
    [KnownType(typeof (StreamRecord))]
    [KnownType(typeof (TerminatorRecord))]
    public abstract class GdbEvent
    {
        [DataMember] public readonly GdbEventType Type;

        protected GdbEvent(GdbEventType type)
        {
            Type = type;
        }
    }
}
﻿using System.Runtime.Serialization;

namespace IdeAl.Communication.Output.Gdb
{
    [DataContract]
    public class StreamRecord : GdbEvent
    {
        [DataMember] public readonly string Message;

        public StreamRecord(string message) : base(GdbEventType.StreamRecord)
        {
            Message = message;
        }

        public override string ToString()
        {
            return Message;
        }
    }
}
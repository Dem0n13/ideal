﻿using System.Runtime.Serialization;

namespace IdeAl.Communication.Output
{
    [DataContract]
    public class CompilerServiceResult : CompletedServiceResult
    {
        [DataMember] public readonly bool IsCompiled;
        [DataMember] public readonly string[] ErrorList;

        public CompilerServiceResult(bool isCompiled, string[] errorList)
        {
            IsCompiled = isCompiled;
            ErrorList = errorList;
        }
    }
}
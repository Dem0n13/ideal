﻿using System.Runtime.Serialization;

namespace IdeAl.Communication.Output
{
    [DataContract]
    public class StartedServiceResult : BaseServiceResult
    {
        [DataMember] public readonly long SessionId;

        public StartedServiceResult(long sessionId) : base(OperationResult.Started)
        {
            SessionId = sessionId;
        }
    }
}
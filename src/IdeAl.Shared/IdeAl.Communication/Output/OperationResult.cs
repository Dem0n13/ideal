﻿namespace IdeAl.Communication.Output
{
    public enum OperationResult
    {
        Started,
        Completed,
        Failed
    }
}
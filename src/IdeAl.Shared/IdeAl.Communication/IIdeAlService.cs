﻿using System.ServiceModel;
using IdeAl.Communication.Input;
using IdeAl.Communication.Output;

namespace IdeAl.Communication
{
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof (IdeAlServiceHelper))]
    public interface IIdeAlService
    {
        [OperationContract]
        BaseServiceResult NotImplementedOperation();

        [OperationContract]
        BaseServiceResult GetEvents();

        [OperationContract]
        BaseServiceResult GetFileList(string directory);

        [OperationContract]
        BaseServiceResult ReadFileContent(string directory, string fileName);

        [OperationContract]
        BaseServiceResult DownloadFileContent(string directory, string fileName);

        [OperationContract]
        BaseServiceResult WriteFileContent(string directory, string fileName, string content);

        [OperationContract]
        BaseServiceResult DeleteFile(string directory, string fileName);

        [OperationContract]
        BaseServiceResult Compile(string directory, string fileName);

        [OperationContract]
        BaseServiceResult StartDebugSession(string directory, string srcFileName);

        [OperationContract]
        BaseServiceResult StopDebugSession(long sessionId);

        [OperationContract]
        BaseServiceResult ExecuteDebugCommand(long sessionId, BaseDebugCommand command);
    }
}
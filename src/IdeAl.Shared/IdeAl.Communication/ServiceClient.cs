﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ServiceModel;
using System.Timers;
using IdeAl.Communication.Output;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;
using NLog;

namespace IdeAl.Communication
{
    public class ServiceClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ChannelFactory<IIdeAlService> _channelFactory;

        private const double DefaultEventsPollingInterval = 100.0;
        private const double MaxEventsPollingInterval = 5000.0;
        private readonly Timer _eventsPollingTimer;
        private IIdeAlService _eventsPollingChannel;

        public event Action ServiceEventsRecieved = () => { };
        public readonly ConcurrentQueue<KeyValuePair<long, GdbEvent[]>> ServiceEventQueue = new ConcurrentQueue<KeyValuePair<long, GdbEvent[]>>();

        public ServiceClient(ChannelFactory<IIdeAlService> channelFactory)
        {
            channelFactory.ThrowIfNull("channelFactory");

            _channelFactory = channelFactory;

            _eventsPollingChannel = _channelFactory.CreateChannel();

            _eventsPollingTimer = new Timer(DefaultEventsPollingInterval) { AutoReset = false };
            _eventsPollingTimer.Elapsed += (sender, args) => RecieveServiceEvents();
        }

        public void StartReceivingServiceEvents()
        {
            _eventsPollingTimer.Enabled = true;
        }

        public void StopReceivingServiceEvents()
        {
            _eventsPollingTimer.Enabled = false;
        }

        [Obsolete("Метод предназначен для отладочных целей. Рекомендуется использовать перегрузку без generic-параметра")]
        public TServiceResult Invoke<TServiceResult>(Func<IIdeAlService, BaseServiceResult> method) where TServiceResult : BaseServiceResult
        {
            var result = Invoke(method);
            switch (result.OperationResult)
            {
                case OperationResult.Started:
                case OperationResult.Completed:
                    return (TServiceResult) result;
                case OperationResult.Failed:
                    throw new Exception(((ExceptionServiceResult)result).Detail.ToString());
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public BaseServiceResult Invoke(Func<IIdeAlService, BaseServiceResult> method)
        {
            method.ThrowIfNull("method");

            var channel = _channelFactory.CreateChannel();
            try
            {
                return method(channel);
            }
            catch (Exception exception)
            {
                Logger.Error("Invoke {0}", exception.Message);
                throw;
            }
            finally
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                CloseChannel((ICommunicationObject) channel);
            }
        }

        private void RecieveServiceEvents()
        {
            try
            {
                var result = _eventsPollingChannel.GetEvents();
                if (result.OperationResult == OperationResult.Completed)
                {
                    var eventPacks = ((ValueServiceResult<Dictionary<long, GdbEvent[]>>) result).Value;
                    foreach (var pair in eventPacks)
                        ServiceEventQueue.Enqueue(pair);

                    ServiceEventsRecieved();
                }

                _eventsPollingTimer.Interval = DefaultEventsPollingInterval;
            }
            catch (Exception exception)
            {
                Logger.Warn("RecieveServiceEvents: {0}", exception.Message);
                // ReSharper disable once SuspiciousTypeConversion.Global
                CloseChannel((ICommunicationObject) _eventsPollingChannel);
                _eventsPollingChannel = _channelFactory.CreateChannel();
                _eventsPollingTimer.Interval = Math.Min(_eventsPollingTimer.Interval*2, MaxEventsPollingInterval);
            }

            _eventsPollingTimer.Enabled = true;
        }

        private void CloseChannel(ICommunicationObject channel)
        {
            try
            {
                Logger.Debug("CloseChannel: " + channel.State);
                channel.Close();
            }
            catch (CommunicationException)
            {
                Logger.Warn("CloseChannel>CommunicationException");
                channel.Abort();
            }
            catch (TimeoutException)
            {
                Logger.Warn("CloseChannel>TimeoutException");
                channel.Abort();
            }
            catch (Exception)
            {
                channel.Abort();
                throw;
            }
        }
    }
}
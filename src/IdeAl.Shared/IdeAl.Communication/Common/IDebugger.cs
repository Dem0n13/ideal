﻿namespace IdeAl.Communication.Common
{
    public interface IDebugger
    {
        void Start();
        void StepInto();
        void StepOver();
        void Continue();
        void Stop();
        void AddBreakpoint(int lineNumber);
        void AddBreakpoint(string label);
        void DeleteBreakpoint(int number);
        void Watch(RegisterGroup registerGroup);
        void Watch(string name, VarType type);
        void Unwatch(RegisterGroup registerGroup);
        void Unwatch(string name);
        void SetValue(string key, string value);
    }
}
namespace IdeAl.Communication.Common
{
    public enum RegisterGroup
    {
        Unknown,
        Cpu64,
        Cpu32,
        Cpu16,
        Cpu8,
        CpuCtrl,
        Sse128,
        SseCtrl,
        Fpu80,
        FpuCtrl
    }
}
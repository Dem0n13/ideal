﻿namespace IdeAl.Communication.Common
{
    public enum VarType
    {
        Auto,
        Register,
        Int8,
        Int16,
        Int32,
        Int64,
        Float32,
        Float64,
        Float80,
        String
    }
}
using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class ContinueCommand : BaseDebugCommand
    {
        public override void Run(IDebugger debugger)
        {
            debugger.Continue();
        }
    }
}
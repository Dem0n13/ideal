using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class StopCommand : BaseDebugCommand
    {
        public override void Run(IDebugger debugger)
        {
            debugger.Stop();
        }
    }
}
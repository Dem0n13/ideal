using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class DeleteBreakpointCommand : BaseDebugCommand
    {
        [DataMember] private readonly int _number;

        public DeleteBreakpointCommand(int number)
        {
            _number = number;
        }

        public override void Run(IDebugger debugger)
        {
            debugger.DeleteBreakpoint(_number);
        }
    }
}
using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class AddBreakpointCommand : BaseDebugCommand
    {
        [DataMember] private readonly string _label;
        [DataMember] private readonly int? _lineNumber;

        public AddBreakpointCommand(int lineNumber)
        {
            _lineNumber = lineNumber;
        }

        public AddBreakpointCommand(string label)
        {
            _label = label;
        }

        public override void Run(IDebugger debugger)
        {
            if (_lineNumber != null)
                debugger.AddBreakpoint(_lineNumber.Value);
            else
                debugger.AddBreakpoint(_label);
        }
    }
}
using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class UnwatchCommand : BaseDebugCommand
    {
        [DataMember] private readonly string _name;
        [DataMember] private readonly RegisterGroup _registerGroup;

        public UnwatchCommand(string name)
        {
            _name = name;
        }

        public UnwatchCommand(RegisterGroup registerGroup)
        {
            _registerGroup = registerGroup;
        }

        public override void Run(IDebugger debugger)
        {
            if (_name == null)
                debugger.Unwatch(_registerGroup);
            else
                debugger.Unwatch(_name);
        }
    }
}
using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class SetValueCommand : BaseDebugCommand
    {
        [DataMember] private readonly string _key;
        [DataMember] private readonly string _value;

        public SetValueCommand(string key, string value)
        {
            _key = key;
            _value = value;
        }

        public override void Run(IDebugger debugger)
        {
            debugger.SetValue(_key, _value);
        }
    }
}
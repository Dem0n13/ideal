﻿using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public abstract class BaseDebugCommand
    {
        public abstract void Run(IDebugger debugger);
    }
}
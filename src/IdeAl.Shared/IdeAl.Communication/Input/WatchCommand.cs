using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class WatchCommand : BaseDebugCommand
    {
        [DataMember] private readonly RegisterGroup _registerGroup;
        [DataMember] private readonly string _name;
        [DataMember] private readonly VarType _type;

        public WatchCommand(RegisterGroup registerGroup)
        {
            _registerGroup = registerGroup;
        }

        public WatchCommand(string name, VarType type)
        {
            _name = name;
            _type = type;
        }

        public override void Run(IDebugger debugger)
        {
            if (_name == null)
                debugger.Watch(_registerGroup);
            else
                debugger.Watch(_name, _type);
        }
    }
}
using System.Runtime.Serialization;
using IdeAl.Communication.Common;

namespace IdeAl.Communication.Input
{
    [DataContract]
    public class StartCommand : BaseDebugCommand
    {
        public override void Run(IDebugger debugger)
        {
            debugger.Start();
        }
    }
}
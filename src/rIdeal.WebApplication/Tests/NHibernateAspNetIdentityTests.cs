﻿using IdeAl.WebApplication.Persistence;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.AspNet.Identity;
using NUnit.Framework;
using rIdeal.WebApplication.DomainEntities;

namespace Tests
{
    [TestFixture]
    public class NHibernateAspNetIdentityTests
    {
        private const string RoleName = "administrators";
        private const string User1Name = "admin1";
        private const string User2Name = "admin2";
        private const string Password = "password";

        private SQLiteDb _db;
        private ISession _session;
        private UserStore<ApplicationUser> _userStore;
        private ApplicationUserManager _userManager;
        private RoleManager<IdentityRole> _roleManager;
        
        [SetUp]
        public void SetUp()
        {
            _db = new SQLiteDb("temp.db3", null);
            _db.CreateSchema();

            _session = _db.OpenSession();
            _userStore = new UserStore<ApplicationUser>(_session);
            _userManager = new ApplicationUserManager(_userStore);
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_session));
        }

        [Test]
        public void CreateAdmin()
        {
            CreateRole(RoleName);
            Assert.IsTrue(_roleManager.RoleExistsAsync(RoleName).Result);

            CreateUserWithRole(User1Name, Password, RoleName);
            var user = _userManager.FindAsync(User1Name, Password).Result;
            Assert.IsNotNull(user);
            Assert.IsNotNull(user.Id);
            CollectionAssert.IsNotEmpty(user.Roles);
        }

        [Test]
        public void RemoveFromRole()
        {
            CreateRole(RoleName);
            CreateUserWithRole(User1Name, Password, RoleName);
            var user = _userManager.FindAsync(User1Name, Password).Result;
            _userManager.RemoveFromRoleAsync(user.Id, RoleName);
            
            user = _userManager.FindAsync(User1Name, Password).Result;
            CollectionAssert.IsEmpty(user.Roles);
            Assert.IsTrue(_roleManager.RoleExistsAsync(RoleName).Result);
        }

        [Test]
        public void RemoveOneOfTwoUser()
        {
            CreateRole(RoleName);
            CreateUserWithRole(User1Name, Password, RoleName);
            CreateUserWithRole(User2Name, Password, RoleName);

            var user = _userManager.FindAsync(User1Name, Password).Result;
            var role = _roleManager.FindByNameAsync(RoleName).Result;

            _userManager.Delete(user);
            user = _userManager.FindAsync(User1Name, Password).Result;
            Assert.IsNull(user);

            user = _userManager.FindAsync(User2Name, Password).Result;
            Assert.IsNotNull(user);
            Assert.IsTrue(_userManager.IsInRoleAsync(user.Id, RoleName).Result);

            role = _roleManager.FindByNameAsync(RoleName).Result;
            Assert.IsNotNull(role);
        }

        private void CreateRole(string roleName)
        {
            _roleManager.CreateAsync(new IdentityRole(roleName));
        }

        private void CreateUserWithRole(string userName, string password, string roleName)
        {
            var user = new ApplicationUser {UserName = userName};
            _userManager.CreateAsync(user, password);
            _userManager.AddToRoleAsync(user.Id, roleName);
        }

        [TearDown]
        public void TearDown()
        {
            _session.Dispose();
        }
    }
}
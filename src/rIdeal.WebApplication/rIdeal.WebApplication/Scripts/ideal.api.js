﻿var api = (function () {
    var $this = $(this);

    var client = $.connection.ideAlHub.client;
    var server = $.connection.ideAlHub.server;

    /* hub events */
    client.endGetFileList = function (list) {
        $this.trigger('file/list/end', [list]);
    };
    client.failCreateFile = function (message) {
        $this.trigger('file/create/fail', message);
    };
    client.endCreateFile = function (fileName) {
        $this.trigger('file/create/end', fileName);
    };
    client.endReadFileContent = function (fileName, content) {
        $this.trigger('file/open/end', [fileName, content]);
    };
    client.endSaveFile = function () {
        $this.trigger('file/save/end');
    };
    client.endCompile = function () {
        $this.trigger('compile/end');
    };
    client.failCompile = function (errors) {
        $this.trigger('compile/fail', [errors]);
    };
    client.failRunning = function (errMessage) {
        $this.trigger('running/fail', errMessage);
    };
    client.endStartDebug = function () {
        $this.trigger('debug/start/end');
    };
    client.endStopDebug = function () {
        $this.trigger('debug/stop/end');
    };
    client.onRunning = function () {
        $this.trigger('debug/running');
    };
    client.onPaused = function () {
        $this.trigger('debug/paused');
    };
    client.onStopped = function () {
        $this.trigger('debug/stopped');
    };
    client.onBreakpointCreated = function (line, number) {
        $this.trigger('debug/breakpointCreated', [line, number]);
    };
    client.onSelectDebugLine = function (line) {
        $this.trigger('debug/selectDebugLine', line);
    };
    client.onVariableCreated = function (name, value) {
        $this.trigger('debug/variableCreated', [name, value]);
    };
    client.onCpuRegisterList = function (regGroup, registers) {
        $this.trigger('debug/cpuRegisterList', [regGroup, registers]);
    };
    client.onCtrlRegisterList = function (registers) {
        $this.trigger('debug/ctrlRegisterList', [registers]);
    };

    /* public */
    this.init = function() {
        $.connection.hub.start().done(function() {
            $this.trigger('ready');
        });
    };
    this.beginCreateFile = function(fileName) {
        server.beginCreateFile(fileName);
    };
    this.beginOpenFile = function(fileName) {
        server.beginReadFileContent(fileName);
    };
    this.beginSaveFile = function(fileName, content) {
        server.beginSaveFile(fileName, content);
    };
    this.beginDeleteFile = function(fileName) {
        server.beginDeleteFile(fileName);
    };
    this.beginGetFileList = function() {
        server.beginGetFileList();
    };
    this.beginCompile = function (fileName, content) {
        server.beginCompile(fileName, content);
    };
    this.beginStartDebug = function (fileName, content, breakpoints) {
        server.beginStartDebug(fileName, content, breakpoints);
    };
    this.beginStopDebug = function () {
        server.beginStopDebug();
    };
    this.beginStepIntoDebug = function () {
        server.beginStepIntoDebug();
    };
    this.beginStepOverDebug = function () {
        server.beginStepOverDebug();
    };
    this.beginContinueDebug = function () {
        server.beginContinueDebug();
    };
    this.beginSetBreakpoint = function (line) {
        server.beginSetBreakpoint(line);
    };
    this.beginDeleteBreakpoint = function (number) {
        server.beginDeleteBreakpoint(number);
    };

    return this;
}).call(api || {});
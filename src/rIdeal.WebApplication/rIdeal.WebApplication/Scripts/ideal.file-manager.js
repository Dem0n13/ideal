﻿var fileManager = (function (api, editor) {
    var $api = $(api);
    var $editor = $(editor);

    var fileList = [];
    var selectedFileIndex = null;
    var openedFileName = null;
    var isOpenedFileDirty = false;
    var isLocked = false;

    // private
    function updateActions() {
        var anyFileSelected = selectedFileIndex !== null;
        var canOpenSelectedFile = anyFileSelected && isFileOpenable(fileList[selectedFileIndex]);

        $('#btn-create-file-window').prop('disabled', isLocked);
        $('#btn-open-file').prop('disabled', isLocked || !canOpenSelectedFile);
        $('#btn-save-file').prop('disabled', isLocked || !isOpenedFileDirty);
        $('#btn-download-file').prop('disabled', !anyFileSelected);
        $('#btn-delete-file').prop('disabled', isLocked || !anyFileSelected);
    }

    function clearSelected() {
        selectedFileIndex = null;
        $('.file-list').children().removeClass('active');
    }

    function setSelected(index) {
        clearSelected();
        selectedFileIndex = index;
        $('.file-list-item:data(index=' + index + ')').addClass('active');
    }

    function isFileOpenable(fileName) {
        return fileName.match(/\.s$/) || fileName.match(/\.txt/);
    }

    function setOpened(fileName) {
        openedFileName = fileName;
        isOpenedFileDirty = false;
    }

    /* module actions */
    $('.file-list').on('click', '.file-list-item', function () {
        setSelected($(this).data('index'));
        updateActions();
    });
    $('#btn-refresh-file-list').click(function () {
        api.beginGetFileList();
    });
    $('#btn-create-file-window').click(function () {
        $('#NewFileName').val("").removeClass('input-validation-error');
        $('#error-creating-file')
            .addClass('validation-summary-valid')
            .removeClass('validation-summary-errors');
        $('#modal-window-create-file').height(220).modal();
    });
    $("#btn-create-file").click(function () {
        if (!$('#formToolbar').valid()) {
            $('#modal-window-create-file').height(250);
        } else {
            api.beginCreateFile($('#NewFileName').val());
        }
    });
    $("#btn-open-file").click(function () {
        api.beginOpenFile(fileList[selectedFileIndex]);
    });
    $("#btn-save-file").click(function () {
        api.beginSaveFile(openedFileName, editor.getContent());
    });
    $('#btn-download-file').click(function () {
        var url = $(this).data('action');
        var fileName = fileList[selectedFileIndex];
        url = url.replace('__fileName__', fileName);

        window.location.href = url;
    });
    $("#btn-delete-file").click(function () {
        api.beginDeleteFile(fileList[selectedFileIndex]);
    });

    /* api events */
    $api.on('file/create/fail', function(e, message) {
        $('#error-creating-file')
            .addClass('validation-summary-errors')
            .removeClass('validation-summary-valid')
            .find("li").removeAttr('style').html(message);

        $('#NewFileName').addClass('input-validation-error');
        $('#modal-window-create-file').height(250);
    });
    $api.on('file/create/end', function (e, fileName) {
        $('#modal-window-create-file').modal('hide');

        var fileIndex = fileList.length;
        fileList.push(fileName);
        $('.file-list').append('<a class="list-group-item file-list-item" data-index="' + fileIndex + '">' + fileName + '</a>');

        setSelected(fileIndex);
        setOpened(fileName);
        updateActions();
    });
    $api.on('file/open/end', function (e, fileName) {
        setOpened(fileName);
        updateActions();
    });
    $api.on('ready', function() {
        api.beginGetFileList();
    });
    $api.on('file/list/end', function (e, list) {
        fileList = list;

        $('.file-list').empty();
        for (var i = 0; i < list.length; i++) {
            $('.file-list').append('<a class="list-group-item file-list-item" data-index="' + i + '">' + list[i] + '</a>');
        }

        clearSelected();
        updateActions();
    });
    $api.on('file/save/end', function () {
        isOpenedFileDirty = false;
        updateActions();
    });
    $api.on('debug/start/end', function () {
        isLocked = true;
        updateActions();
    });
    $api.on('debug/stopped', function () {
        isLocked = false;
        updateActions();
    });
    $api.on('debug/stop/end', function () {
        isLocked = false;
        updateActions();
    });

    /* editor events */
    $editor.on('content/changed', function() {
        isOpenedFileDirty = true;
        updateActions();
    });

    return this;
}).call(fileManager || {}, api || {}, editor || {});
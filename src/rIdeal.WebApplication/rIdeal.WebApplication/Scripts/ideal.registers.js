﻿var registers = (function (api) {
    var $this = $(this);
    var $api = $(api);

    var cpuRegisters = [];
    var ctrlRegisters = [];

    /* api events */
    $api.on('debug/cpuRegisterList', function (e, regGroup, registers) {
        cpuRegisters = registers;
        $('#capacity').children().removeClass('active');
        $('.register-item').remove();

        $('#btn-' + regGroup.toLowerCase()).addClass('active');
        registers.forEach(function (register) {
            $('#registers-cpu').append('<tr class="register-item"><td id="name-' + register + '">' + register + '</td><td id="value-' + register + '"></td></tr>');
        });
    });
    $api.on('debug/ctrlRegisterList', function (e, registers) {
        ctrlRegisters = registers;
        $('.register-ctrl-item').remove();
        registers.forEach(function (register) {
            $('#registers-ctrl').append('<tr class="register-ctrl-item"><td id="name-' + register + '">' + register + '</td><td id="value-' + register + '"></td></tr>');
        });
    });
    $api.on('debug/variableCreated', function (e, name, value) {
        if ($.inArray(name, cpuRegisters) != -1 || $.inArray(name, ctrlRegisters) != -1)
            $('#value-' + name).html(value);
    });

    return this;
}).call(registers || {}, api || {});
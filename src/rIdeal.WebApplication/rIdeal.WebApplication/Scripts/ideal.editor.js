﻿var editor = (function (api) {
    var $this = $(this);
    var $api = $(api);

    var openedFileName = null;
    var isDirtyCheckEnabled = null;
    var currentDebugLine = null;
    var breakpoints = [];

    var aceEditor = ace.edit("editor");
    aceEditor.setFontSize(14);
    aceEditor.getSession().setMode("ace/mode/assembly_x86");
    aceEditor.on("gutterclick", function (e) {
        var target = e.domEvent.target;
        if (target.className.indexOf("ace_gutter-cell") == -1)
            return;
        var line = e.getDocumentPosition().row;
        if (target.className.indexOf('ace_breakpoint') == -1) {
            aceEditor.getSession().setBreakpoint(line, 'ace_breakpoint');
            api.beginSetBreakpoint(line);
        } else {
            aceEditor.getSession().clearBreakpoint(line);
            api.beginDeleteBreakpoint(breakpoints[line + 1]);
        }
    });
    aceEditor.getSession().on('change', function () {
        if (isDirtyCheckEnabled)
            markContentAsDirty(true);
    });
    aceEditor.getSession().on('changeScrollTop', function () {
        if (currentDebugLine != null)
            setDebugLine(currentDebugLine);
    });

    /* private */
    function showInEditor(fileName, content) {
        $('#editor-container').removeClass('hidden');
        $('#alertCurrentFile').html(fileName);
        openedFileName = fileName;

        isDirtyCheckEnabled = false;
        aceEditor.setValue(content);
        isDirtyCheckEnabled = true;

        aceEditor.gotoLine(0);
        aceEditor.getSession().clearBreakpoints();
        breakpoints = [];
        aceEditor.focus();

        markContentAsDirty(false);
        clearResultPanel();
    }

    function setDebugLine(line) {
        var offset = $('.ace_gutter-cell').first().html().replace(/\<.*/, '') - 1;
        $('.ace_text-layer .ace_line').removeClass('ace_step_debug');
        $('.ace_text-layer .ace_line:nth-child(' + (line - offset) + ')').addClass('ace_step_debug');
    };

    function clearResultPanel() {
        $('#result-compile').children().remove();
        $('#error-compile').children().remove();
    };

    function markContentAsDirty(isDirty) {
        if (isDirty) {
            $('#alertCurrentFile').html(openedFileName + '*');
            $this.trigger('content/changed');
        } else {
            $('#alertCurrentFile').html(openedFileName);
        }
    }

    function setDebugState(isDebugging, isRunning) {
        aceEditor.focus();
        aceEditor.setReadOnly(isDebugging);
        if (isDebugging && !isRunning)
            $('.registers').toggleClass('hidden', false);
        if (!isDebugging && !isRunning)
            $('.registers').toggleClass('hidden', true);
        $('.btn-not-debugging').toggleClass('hidden', isDebugging); // не доступны при отладке
        $('.btn-debugging-not-running').toggleClass('hidden', !isDebugging || isRunning); // не доступны при разработке и во время выполнения
        $('.btn-debugging-all').toggleClass('hidden', !isDebugging); // не доступны только при разработке
        if (!isDebugging) {
            $('.ace_text-layer .ace_line').removeClass('ace_step_debug');
            currentDebugLine = null;
        }
    }

    /* module actions */
    $("#btn-compile").click(function () {
        api.beginCompile(openedFileName, aceEditor.getValue());
    });
    $("#btn-start-debug").click(function () {
        api.beginStartDebug(openedFileName, aceEditor.getValue(), aceEditor.getSession().getBreakpoints());
    });
    $("#btn-stop-debug").click(function () {
        api.beginStopDebug();
    });
    $("#btn-step-into-debug").click(function () {
        api.beginStepIntoDebug();
    });
    $("#btn-step-over-debug").click(function () {
        api.beginStepOverDebug();
    });
    $("#btn-continue-debug").click(function () {
        api.beginContinueDebug();
    });

    /* api events */
    $api.on('file/create/end', function (e, fileName) {
        showInEditor(fileName, '');
    });
    $api.on('file/open/end', function (e, fileName, content) {
        showInEditor(fileName, content);
    });
    $api.on('file/save/end', function () {
        markContentAsDirty(false);
    });
    $api.on('compile/end', function () {
        clearResultPanel();
        $('#result-compile').append('<li>Успешно</li>');
    });
    $api.on('compile/fail', function (e, errors) {
        clearResultPanel();
        $('#result-compile').append('<li>Ошибка компиляции</li>');
        errors.forEach(function (error) {
            $('#error-compile').append('<li>' + error + '</li>');
        });
    });
    $api.on('running/fail', function (e, errMessage) {
        clearResultPanel();
        $('#result-compile').append('<li>' + errMessage + '</li>');
    });
    $api.on('debug/start/end', function () {
        setDebugState(true, true);
    });
    $api.on('debug/stop/end', function () {
        setDebugState(false, false);
    });
    $api.on('debug/running', function () {
        setDebugState(true, true);
    });
    $api.on('debug/paused', function () {
        setDebugState(true, false);
    });
    $api.on('debug/stopped', function () {
        setDebugState(false, false);
    });
    $api.on('debug/breakpointCreated', function (e, line, number) {
        breakpoints[line] = number;
    });
    $api.on('debug/selectDebugLine', function (e, line) {
        aceEditor.gotoLine(line);
        setDebugLine(line);
        currentDebugLine = line;
    });

    /* public */
    this.getContent = function () {
        return aceEditor.getValue();
    };
    this.setContent = function (value) {
        aceEditor.setValue(value);
    };

    /*hot keys*/
    aceEditor.commands.addCommand({
        name: 'start_debug',
        bindKey: { win: 'Ctrl-F9' },
        exec: function () {
            api.beginStartDebug(openedFileName, aceEditor.getValue(), aceEditor.getSession().getBreakpoints());
        },
        readOnly: false
    });
    aceEditor.commands.addCommand({
        name: 'stop_debug',
        bindKey: { win: 'Shift-F9' },
        exec: function () {
            api.beginStopDebug();
        },
        readOnly: true
    });
    aceEditor.commands.addCommand({
        name: 'step_over',
        bindKey: { win: 'F7' },
        exec: function () {
            api.beginStepOverDebug();
        },
        readOnly: true // false, если мы не хотим чтобы в readOnly работало
    });
    aceEditor.commands.addCommand({
        name: 'step_into',
        bindKey: { win: 'F8' },
        exec: function () {
            api.beginStepIntoDebug();
        },
        readOnly: true
    });
    aceEditor.commands.addCommand({
        name: 'continue_debug',
        bindKey: { win: 'F9' },
        exec: function () {
            api.beginContinueDebug();
        },
        readOnly: true
    });

    return this;
}).call(editor || {}, api || {});
﻿using IdeAl.FrameworkExtensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace IdeAl.WebApplication.FrameworkExtensions
{
    public class PrincipalUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            request.ThrowIfNull("request");

            return request.User == null ? null : request.User.Identity.GetUserId();
        }
    }
}
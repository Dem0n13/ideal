﻿using System;
using System.Reflection;
using System.Web.Mvc;

namespace rIdeal.WebApplication.FrameworkExtensions
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class HttpActionAttribute : ActionNameSelectorAttribute
    {
        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            // default naming: Form.Action == Controller.Action
            if (actionName.Equals(methodInfo.Name, StringComparison.OrdinalIgnoreCase))
                return true;

            // filtering: only Form.Action == "Action" is allowed
            if (!actionName.Equals("Action", StringComparison.OrdinalIgnoreCase))
                return false;

            // get value of submit button by Controller.Action name
            var request = controllerContext.RequestContext.HttpContext.Request;
            return request[methodInfo.Name] != null;
        }
    }
}
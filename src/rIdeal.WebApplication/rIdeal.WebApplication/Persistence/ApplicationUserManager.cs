﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IdeAl.WebApplication.FrameworkExtensions;
using Microsoft.AspNet.Identity;
using NHibernate.AspNet.Identity;
using rIdeal.WebApplication.DomainEntities;

namespace IdeAl.WebApplication.Persistence
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(UserStore<ApplicationUser> store) : base(store)
        {
        }

        public Task<IList<ApplicationUser>> GetAllAsync()
        {
            return ((UserStore<ApplicationUser>) Store).GetAllAsync();
        }
        public Task DeleteAsync(ApplicationUser user)
        {
            return Store.DeleteAsync(user);
        }
    }

    public static class ApplicationUserManagerExtensions
    {
        public static IList<ApplicationUser> GetAll(this ApplicationUserManager manager)
        {
            return AsyncHelper.RunSync(manager.GetAllAsync);
        }

        public static void Delete(this ApplicationUserManager manager, ApplicationUser user)
        {
            if (manager == null)
                throw new ArgumentNullException("manager");

            AsyncHelper.RunSync(() => manager.DeleteAsync(user));
        }
    }
}
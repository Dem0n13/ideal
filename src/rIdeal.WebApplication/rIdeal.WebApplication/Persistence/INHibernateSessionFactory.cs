using NHibernate;

namespace IdeAl.WebApplication.Persistence
{
    public interface INHibernateSessionFactory
    {
        ISession OpenSession();
    }
}
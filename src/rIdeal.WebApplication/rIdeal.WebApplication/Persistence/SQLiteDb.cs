﻿using System;
using System.Data.SQLite;
using IdeAl.FrameworkExtensions;
using NHibernate;
using NHibernate.AspNet.Identity;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Tool.hbm2ddl;
using rIdeal.WebApplication.Persistence;
using rIdeal.WebApplication.Persistence.Mappings;

namespace IdeAl.WebApplication.Persistence
{
    public class SQLiteDb : INHibernateSessionFactory
    {
#if DEBUG
        private const bool ShowSql = true;
#else
        private const bool ShowSql = false;
#endif

        private readonly Configuration _configuration;
        private readonly ISessionFactory _sessionFactory;

        public SQLiteDb(string dataSource, string password)
            : this(BuildConnectionString(dataSource, password))
        {
        }

        public SQLiteDb(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Data source must not be empty", "connectionString");

            _configuration = new Configuration()
                .SetNamingStrategy(ImprovedNamingStrategy.Instance)
                .DataBaseIntegration(properties =>
                {
                    properties.Driver<SQLite20Driver>();
                    properties.Dialect<SQLiteDialect>();
                    properties.ConnectionString = connectionString;
                    properties.LogSqlInConsole = ShowSql;
                    properties.LogFormattedSql = ShowSql;
                });

            var mapper = new ModelMapper();
            mapper.AddMapping<IdentityUserMapping>();
            mapper.AddMapping<IdentityRoleMapping>();
            mapper.AddMapping<IdentityUserClaimMapping>();
            mapper.AddMapping<ApplicationUserMapping>();
            mapper.AddMappings(typeof(ClassMapping<>).GetDerivedTypes());

            _configuration.AddDeserializedMapping(mapper.CompileMappingForAllExplicitlyAddedEntities(), null);
            _sessionFactory = _configuration.BuildSessionFactory();
        }

        public void CreateSchema()
        {
            var schemaExport = new SchemaExport(_configuration);
            schemaExport.Create(ShowSql, true);
        }

        public void UpdateSchema()
        {
            var schemaUpdate = new SchemaUpdate(_configuration);
            schemaUpdate.Execute(ShowSql, true);
        }

        public ISession OpenSession()
        {
            return _sessionFactory.OpenSession();
        }
 
        public static string BuildConnectionString(string dataSource, string password)
        {
            if (dataSource == null)
                throw new ArgumentNullException("dataSource");

            return new SQLiteConnectionStringBuilder
                   {
                       DataSource = dataSource,
                       Version = 3,
                       Password = password
                   }.ToString();
        }
    }
}

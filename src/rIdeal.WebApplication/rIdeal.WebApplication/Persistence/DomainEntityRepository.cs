using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IdeAl.WebApplication.DomainEntities;
using NHibernate;
using NHibernate.Linq;

namespace IdeAl.WebApplication.Persistence
{
    public class DomainEntityRepository<TEntity, TId> : ICollection<TEntity>, IQueryable<TEntity>, IDisposable
        where TEntity : BaseDomainEntity<TId>, new()
    {
        protected readonly ISession Session;

        public DomainEntityRepository(ISession session)
        {
            if (session == null)
                throw new ArgumentNullException("session");
            if (!session.IsOpen)
                throw new ArgumentException("ISession object must be opened", "session");

            Session = session;
        }

        #region ICollection<TEntity> Members

        public void Add(TEntity entity)
        {
            Transaction(() => Session.Save(entity));
        }

        public void Clear()
        {
            var query = string.Format("from {0} entity", typeof (TEntity));
            Transaction(() => Session.Delete(query));
        }

        public bool Contains(TEntity item)
        {
            return Get(item.Id) != null;
        }

        void ICollection<TEntity>.CopyTo(TEntity[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        public bool Remove(TEntity entity)
        {
            Transaction(() => Session.Delete(entity));
            return true;
        }

        public int Count
        {
            get { return Transaction(() => Session.Query<TEntity>().Count()); }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return Session.Query<TEntity>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            Session.Close();
        }

        #endregion

        #region IQueryable<TEntity> Members

        public Expression Expression
        {
            get { return Session.Query<TEntity>().Expression; }
        }

        public Type ElementType
        {
            get { return Session.Query<TEntity>().ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return Session.Query<TEntity>().Provider; }
        }

        #endregion

        public TEntity Get(TId id)
        {
            return Transaction(() => Session.Get<TEntity>(id));
        }

        public void SaveChanges()
        {
            Session.Flush();
        }

        protected void Transaction(Action command)
        {
            using (var transaction = Session.BeginTransaction())
            {
                command();
                transaction.Commit();
            }
        }

        protected TOut Transaction<TOut>(Func<TOut> command)
        {
            using (var transaction = Session.BeginTransaction())
            {
                var result = command();
                transaction.Commit();
                return result;
            }
        }
    }
}
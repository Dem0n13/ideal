﻿using NHibernate.Mapping.ByCode.Conformist;
using rIdeal.WebApplication.DomainEntities;
using rIdeal.WebApplication.Models;

namespace rIdeal.WebApplication.Persistence.Mappings
{
    public class ApplicationUserMapping : JoinedSubclassMapping<ApplicationUser>
    {
        public ApplicationUserMapping()
        {
            Lazy(false);
            Property(user => user.Path);
        }
    }
}
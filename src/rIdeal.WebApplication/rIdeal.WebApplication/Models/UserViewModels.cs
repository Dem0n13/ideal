﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace rIdeal.WebApplication.Models
{
    public class UserViewModels
    {
        public string UserId { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "{0} должно быть не более {1} символов.")]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }
        
        [StringLength(50, ErrorMessage = "{0} должен быть в пределах от {2} до {1} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Роль")]
        public string Role { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;

namespace IdeAl.WebApplication.Models
{
    public class FileManagerViewModels
    {
        [Required(ErrorMessage = "Поле \"{0}\" должно быть заполнено.")]
        [StringLength(50, ErrorMessage = "{0} должно быть не более {1} символов.")]
        [RegularExpression(@"[^ \/:*?""<>|]+", ErrorMessage = "Имя файла не должно содержать следующих знаков: \\ / : * ? \" < > |")]
        [Display(Name = "Имя файла")]
        public string NewFileName { get; set; }
    }
}
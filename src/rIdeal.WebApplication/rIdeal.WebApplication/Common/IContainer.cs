﻿using System;

namespace IdeAl.WebApplication.Common
{
    public interface IContainer
    {
        void Register<TService, TImplementation>(bool perScopeLifetime = false) where TImplementation : TService;
        void Register<TService>(Func<TService> factory, bool perScopeLifetime = false);
        void Register<TService>(TService instance);
        TService Resolve<TService>();
    }
}
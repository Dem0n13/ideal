﻿using Microsoft.AspNet.SignalR;

namespace IdeAl.WebApplication.Common
{
    public interface ISignalRContainer : IDependencyResolver, IContainer
    {
        void RegisterHubs();
    }
}
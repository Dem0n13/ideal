using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using IdeAl.FrameworkExtensions;
using LightInject;
using Microsoft.AspNet.SignalR;

namespace IdeAl.WebApplication.Common
{
    public class LightInjectContainerAdapter : DefaultDependencyResolver, ISignalRContainer, IMvcContainer
    {
        private readonly ServiceContainer _container = new ServiceContainer();

        public void RegisterHubs()
        {
            foreach (var hub in typeof (Hub).GetDerivedTypes(Assembly.GetExecutingAssembly()))
                _container.Register(hub);
        }

        public override object GetService(Type serviceType)
        {
            try
            {
                return _container.TryGetInstance(serviceType) ?? base.GetService(serviceType);
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.GetAllInstances(serviceType).Concat(base.GetServices(serviceType));
        }

        public void RegisterControllers()
        {
            _container.RegisterControllers();
            _container.EnableMvc();
        }

        public void Register<TService, TImplementation>(bool perScopeLifetime = false) where TImplementation : TService
        {
            if (perScopeLifetime)
                _container.Register<TService, TImplementation>(new PerScopeLifetime());
            else
                _container.Register<TService, TImplementation>();
        }

        public void Register<T>(Func<T> factory, bool perScopeLifetime)
        {
            if (perScopeLifetime)
                _container.Register(serviceFactory => factory(), new PerScopeLifetime());
            else
                _container.Register(serviceFactory => factory());
        }

        public void Register<TService>(TService instance)
        {
            _container.RegisterInstance(instance);
        }

        public TService Resolve<TService>()
        {
            return _container.GetInstance<TService>();
        }
    }
}
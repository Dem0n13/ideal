﻿namespace IdeAl.WebApplication.Common
{
    public interface IMvcContainer
    {
        void RegisterControllers();
    }
}
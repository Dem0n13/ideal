﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IdeAl.WebApplication.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using NHibernate;
using NHibernate.AspNet.Identity;
using rIdeal.WebApplication.DomainEntities;
using rIdeal.WebApplication.FrameworkExtensions;
using rIdeal.WebApplication.Models;

namespace IdeAl.WebApplication.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    public class UsersController : Controller
    {
        private readonly ApplicationUserManager _userManager;

        public UsersController(ISession session)
        {
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(session));
            ViewBag.Users = _userManager.GetAll();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, HttpAction]
        public ActionResult Delete(string userId)
        {
            var user = _userManager.FindById(userId);

            if (user != null)
            {
                _userManager.Delete(user);
                ViewBag.Users = _userManager.GetAll();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(string userId)
        {
            var user = _userManager.FindById(userId);

            if (user != null)
            {
                ViewData.Model = new UserViewModels
                                 {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Password = "",
                    ConfirmPassword = "",
                    Role = user.Roles.First().Name
                };

                return View();
            }

            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModels model)
        {
            if (ModelState.IsValid)
            {
                var user = _userManager.FindById(model.UserId);

                if (user != null)
                {
                    user.UserName = model.UserName;
                    user.PasswordHash = string.IsNullOrEmpty(model.Password) ? user.PasswordHash : new PasswordHasher().HashPassword(model.Password);
                    user.Roles.Clear();

                    _userManager.AddToRole(user.Id, model.Role);

                    if (user.Id == User.Identity.GetUserId())
                    {
                        SignIn(user, false);
                    }

                    return RedirectToAction("Index", "Users");
                }
            }

            return View(model);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        
        private void SignIn(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = _userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }
    }
}
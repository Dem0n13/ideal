﻿using System.Net.Mime;
using System.Web.Mvc;
using IdeAl.WebApplication.Hubs;
using Microsoft.AspNet.Identity;

namespace IdeAl.WebApplication.Controllers
{
    [Authorize]
    public class FileManagerController : Controller
    {
        private readonly ApplicationLogic _application;

        public FileManagerController(ApplicationLogic application)
        {
            _application = application;
        }

        [HttpGet]
        public ActionResult _ToolbarPartial()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult Download(string fileName)
        {
            var data = _application.DownloadFile(User.Identity.GetUserId(), fileName);
            if (data == null)
                return HttpNotFound(string.Format("Не удалось получить файл с именем '{0}' ", fileName));
            return File(data, MediaTypeNames.Application.Octet, fileName);
        }
	}
}
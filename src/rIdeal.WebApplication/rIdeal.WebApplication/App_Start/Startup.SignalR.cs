﻿using IdeAl.WebApplication.Common;
using IdeAl.WebApplication.FrameworkExtensions;
using Microsoft.AspNet.SignalR;
using Owin;

namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        private void ConfigureSignalR(IAppBuilder app, ISignalRContainer container)
        {
            container.RegisterHubs();
            container.Register<IUserIdProvider>(new PrincipalUserIdProvider());
            GlobalHost.DependencyResolver = container;

            var config = new HubConfiguration {Resolver = container};
            app.MapSignalR(config);
        }
    }
}

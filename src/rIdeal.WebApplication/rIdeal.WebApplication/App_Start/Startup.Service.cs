﻿using System.ServiceModel;
using IdeAl.Communication;
using IdeAl.WebApplication.Common;

namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        private void ConfigureService(IContainer container)
        {
            var channelFactory = new ChannelFactory<IIdeAlService>(
                new NetNamedPipeBinding(),
                new EndpointAddress("net.pipe://localhost/IdeAlService"));

            container.Register(new ServiceClient(channelFactory));
        }
    }
}
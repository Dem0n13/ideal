﻿using System.Web.Configuration;
using IdeAl.WebApplication.Common;
using IdeAl.WebApplication.Persistence;
using Microsoft.AspNet.Identity;
using NHibernate.AspNet.Identity;
using rIdeal.WebApplication.DomainEntities;

namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        public void ConfigureDatabase(IContainer container)
        {
            // строка подключения
            string connectionString = null;
            var webConfig = WebConfigurationManager.OpenWebConfiguration("/WebSite");
            if (webConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            {
                var defaultConnection = webConfig.ConnectionStrings.ConnectionStrings["DefaultConnection"];
                if (defaultConnection != null)
                    connectionString = defaultConnection.ConnectionString;
            }

            // инициализация
            var db = connectionString == null ? new SQLiteDb("database.db3", null) : new SQLiteDb(connectionString);
            db.UpdateSchema();

            // начальное заполнение
            using (var session = db.OpenSession())
            {
                // роли
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(session));
                foreach (var roleName in Roles.GetAll())
                {
                    if (!roleManager.RoleExists(roleName))
                    {
                        roleManager.Create(new IdentityRole(roleName));
                    }
                }

                // учетная запись администратора
                var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(session));
                const string adminName = "admin";
                const string adminPass = "111111";

                var result = userManager.FindByName(adminName);

                if (result == null)
                {
                    var user = new ApplicationUser {UserName = adminName};
                    userManager.Create(user, adminPass);
                    userManager.AddToRole(user.Id, Roles.Admin);
                }
            }

            // регистрация в контейнере
            container.Register<INHibernateSessionFactory>(db);
            container.Register(db.OpenSession, true);
        }
    }
}
﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        private void ConfigureNLog()
        {
            var config = new LoggingConfiguration();

            // targets
            var debuggerTarget = new DebuggerTarget();
            config.AddTarget("debug", debuggerTarget);

            // rules
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, debuggerTarget));

            LogManager.Configuration = config;
        }
    }
}

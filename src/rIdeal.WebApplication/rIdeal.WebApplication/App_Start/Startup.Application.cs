﻿using IdeAl.Communication;
using IdeAl.WebApplication.Common;
using IdeAl.WebApplication.Hubs;
using IdeAl.WebApplication.Persistence;

namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        private void StartApplication(IContainer container)
        {
            var serviceClient = container.Resolve<ServiceClient>();
            var sessionFactory = container.Resolve<INHibernateSessionFactory>();
            var applicatonLogic = new ApplicationLogic(serviceClient, sessionFactory);
            container.Register(applicatonLogic);
        }
    }
}

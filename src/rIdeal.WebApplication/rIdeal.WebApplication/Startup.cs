﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using IdeAl.WebApplication;
using IdeAl.WebApplication.Common;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace IdeAl.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = new LightInjectContainerAdapter();
            container.RegisterControllers();
            
            ConfigureAuth(app);
            ConfigureDatabase(container);
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterBundles(BundleTable.Bundles);
            ConfigureSignalR(app, container);
            ConfigureService(container);
            ConfigureNLog();

            StartApplication(container);
        }
    }
}

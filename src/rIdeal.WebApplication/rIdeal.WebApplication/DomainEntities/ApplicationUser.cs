﻿using NHibernate.AspNet.Identity;

namespace rIdeal.WebApplication.DomainEntities
{
    public class ApplicationUser : IdentityUser
    {
        public string Path { get; set; }
    }
}
namespace IdeAl.WebApplication.DomainEntities
{
    public abstract class BaseDomainEntity<TId>
    {
        public TId Id { get; protected set; }
    }
}
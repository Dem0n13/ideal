﻿using System.Collections.Generic;

namespace rIdeal.WebApplication.DomainEntities
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string User = "User";

        public static IEnumerable<string> GetAll()
        {
            yield return Admin;
            yield return User;
        }
    }
}
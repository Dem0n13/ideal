﻿using System.Threading.Tasks;
using IdeAl.Communication;
using IdeAl.FrameworkExtensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace IdeAl.WebApplication.Hubs
{
    [Authorize]
    public class IdeAlHub : Hub
    {
        private static readonly IHubContext StaticContext = GlobalHost.ConnectionManager.GetHubContext<IdeAlHub>();
        private readonly ApplicationLogic _application;
        
        public IdeAlHub(ServiceClient client, ApplicationLogic application)
        {
            client.ThrowIfNull("client");
            application.ThrowIfNull("application");

            _application = application;
        }

        public override Task OnConnected()
        {
            _application.OnConnected(Context.User.Identity.GetUserId(), Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            _application.OnDisconnected(Context.ConnectionId);
            return base.OnDisconnected();
        }

        public void BeginCreateFile(string fileName)
        {
            _application.CreateFile(Context.ConnectionId, fileName);
        }

        public static void EndCreateFile(string userId, string fileName)
        {
            StaticContext.Clients.User(userId).EndCreateFile(fileName);
        }

        public static void FailCreateFile(string connectionId, string message)
        {
            StaticContext.Clients.Client(connectionId).FailCreateFile(message);
        }

        public void BeginReadFileContent(string fileName)
        {
            _application.ReadFileContent(Context.ConnectionId, fileName);
        }
        
        public static void EndReadFileContent(string connectionId, string fileName, string content)
        {
            StaticContext.Clients.Client(connectionId).EndReadFileContent(fileName, content);
        }

        public void BeginGetFileList()
        {
            _application.GetFileList(Context.ConnectionId);
        }

        public static void EndGetFileList(string userId, string[] fileList)
        {
            StaticContext.Clients.User(userId).EndGetFileList(fileList);
        }

        public void BeginSaveFile(string fileName, string content)
        {
            _application.SaveFile(Context.ConnectionId, fileName, content);
        }

        public static void EndSaveFile(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndSaveFile();
        }

        public void BeginDeleteFile(string fileName)
        {
            _application.DeleteFile(Context.ConnectionId, fileName);
            BeginGetFileList();
        }

        public void BeginCompile(string fileName, string content)
        {
            BeginSaveFile(fileName, content);
            _application.Compile(Context.ConnectionId, fileName);
        }

        public static void EndCompile(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndCompile();
        }

        public static void FailCompile(string connectionId, string[] errors)
        {
            StaticContext.Clients.Client(connectionId).FailCompile(errors);
        }

        public static void FailRunning(string connectionId, string errMessage)
        {
            StaticContext.Clients.Client(connectionId).FailRunning(errMessage);
        }

        public void BeginStartDebug(string fileName, string content, string[] breakpoints)
        {
            BeginSaveFile(fileName, content);
            _application.StartDebug(Context.ConnectionId, fileName, breakpoints);
        }

        public static void EndStartDebug(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndStartDebug();
        }

        public static void FailStartDebug(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).FailStartDebug();
        }

        public void BeginContinueDebug()
        {
            _application.ContinueDebug(Context.ConnectionId);
        }

        public void BeginStopDebug()
        {
            _application.StopDebug(Context.ConnectionId);
        }

        public static void EndStopDebug(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndStopDebug();
        }

        public void BeginStepOverDebug()
        {
            _application.StepOverDebug(Context.ConnectionId);
        }

        public static void EndStepOverDebug(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndStepOver();
        }

        public void BeginStepIntoDebug()
        {
            _application.StepIntoDebug(Context.ConnectionId);
        }

        public void BeginSetBreakpoint(int line)
        {
            _application.SetBreakpoint(Context.ConnectionId, line);
        }

        public void BeginDeleteBreakpoint(int number)
        {
            _application.DeleteBreakpoint(Context.ConnectionId, number);
        }

        public static void EndStepIntoDebug(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).EndStepInto();
        }

        // events
        public static void OnRegisterList(string connectionId, string regGroup, string[] registers)
        {
            StaticContext.Clients.Client(connectionId).OnCpuRegisterList(regGroup, registers);
        }

        public static void OnRegisterList(string connectionId, string[] registers)
        {
            StaticContext.Clients.Client(connectionId).OnCtrlRegisterList(registers);
        }

        public static void OnVariableCreated(string connectionId, string name, string value)
        {
            StaticContext.Clients.Client(connectionId).OnVariableCreated(name, value);
        }

        public static void OnRunning(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).OnRunning();
        }

        public static void OnPaused(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).OnPaused();
        }

        public static void OnStopped(string connectionId)
        {
            StaticContext.Clients.Client(connectionId).OnStopped();
        }

        public static void OnBreakpointCreated(string connectionId, string line, string number)
        {
            StaticContext.Clients.Client(connectionId).OnBreakpointCreated(line, number);
        }
        public static void OnSelectDebugLine(string connectionId, string line)
        {
            StaticContext.Clients.Client(connectionId).OnSelectDebugLine(line);
        }
    }
}
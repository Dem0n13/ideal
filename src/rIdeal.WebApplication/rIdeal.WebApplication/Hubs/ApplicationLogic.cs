﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IdeAl.Communication;
using IdeAl.Communication.Common;
using IdeAl.Communication.Input;
using IdeAl.Communication.Output;
using IdeAl.Communication.Output.Gdb;
using IdeAl.FrameworkExtensions;
using IdeAl.WebApplication.Persistence;
using NHibernate.Linq;
using NLog;

namespace IdeAl.WebApplication.Hubs
{
    public class ApplicationLogic
    {
        private const string SrcBaseDirectory = "src\\";
        private const string SrcFileExt = ".s";
        private const string HexFileExt = ".hex";
        private const string TxtFileExt = ".txt";

        private static readonly string[] KnownFileExtensions = { SrcFileExt, HexFileExt, TxtFileExt };
        private static readonly string[] OpenableFileExtensions = { SrcFileExt, TxtFileExt };
        private static readonly string[] DownloadableFileExtensions = { SrcFileExt, HexFileExt, TxtFileExt };

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ServiceClient _serviceClient;
        private readonly ConcurrentDictionary<string, string> _userIdByConnectionId = new ConcurrentDictionary<string, string>();
        private readonly ConcurrentDictionary<long, string> _connectionIdBySessionId = new ConcurrentDictionary<long, string>();
        private readonly ConcurrentDictionary<string, long> _sessionIdByConnectionId = new ConcurrentDictionary<string, long>();
        private readonly INHibernateSessionFactory _sessionFactory;

        public ApplicationLogic(ServiceClient serviceClient, INHibernateSessionFactory sessionFactory)
        {
            serviceClient.ThrowIfNull("serviceClient");
            sessionFactory.ThrowIfNull("sessionFactory");

            _serviceClient = serviceClient;
            _sessionFactory = sessionFactory;

            _serviceClient.ServiceEventsRecieved += ProcessServiceEvents;
            _serviceClient.StartReceivingServiceEvents();
        }

        public void OnConnected(string userId, string connectionId)
        {
            userId.ThrowIfNullOrWhiteSpace("userId");
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            _userIdByConnectionId[connectionId] = userId;
        }

        public void OnDisconnected(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            string deleted;
            _userIdByConnectionId.TryRemove(connectionId, out deleted);
        }

        public void CreateFile(string connectionId, string fileName)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;
            fileName += SrcFileExt;

            var fileListResult = _serviceClient.Invoke<ValueServiceResult<string[]>>(service => service.GetFileList(directory));
            var fileExists = fileListResult.Value.Any(current => fileName.Equals(current, StringComparison.OrdinalIgnoreCase));
            if (fileExists)
            {
                IdeAlHub.FailCreateFile(connectionId, "Файл с таким именем уже существует.");
            }
            else
            {
                _serviceClient.Invoke<CompletedServiceResult>(service => service.WriteFileContent(directory, fileName, ""));
                IdeAlHub.EndCreateFile(userId, fileName);
            }
        }

        public void ReadFileContent(string connectionId, string fileName)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            var serviceResult = _serviceClient.Invoke<ValueServiceResult<string>>(service => service.ReadFileContent(directory, fileName));
            IdeAlHub.EndReadFileContent(connectionId, fileName, serviceResult.Value);
        }

        public byte[] DownloadFile(string userId, string fileName)
        {
            userId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var extension = Path.GetExtension(fileName);
            if (!DownloadableFileExtensions.Any(s => s.Equals(extension)))
                return null;

            var directory = SrcBaseDirectory + userId;

            var serviceResult = _serviceClient.Invoke(service => service.DownloadFileContent(directory, fileName));
            return serviceResult.OperationResult == OperationResult.Completed
                ? serviceResult.As<ValueServiceResult<byte[]>>().Value
                : null;
        }

        public void GetFileList(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            var serviceResult = _serviceClient.Invoke<ValueServiceResult<string[]>>(service => service.GetFileList(directory));
            var filtered = serviceResult.Value
                .Where(file =>
                       {
                           var extension = Path.GetExtension(file);
                           return KnownFileExtensions.Any(s => s.Equals(extension, StringComparison.OrdinalIgnoreCase));
                       })
                .ToArray();

            IdeAlHub.EndGetFileList(userId, filtered);
        }

        public void SaveFile(string connectionId, string fileName, string content)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");
            content.ThrowIfNull("content");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            _serviceClient.Invoke<BaseServiceResult>(service => service.WriteFileContent(directory, fileName, content));

            IdeAlHub.EndSaveFile(connectionId);
        }

        public void DeleteFile(string connectionId, string fileName)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            _serviceClient.Invoke<BaseServiceResult>(service => service.DeleteFile(directory, fileName));
        }

        public void Compile(string connectionId, string fileName)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            var compileResult = _serviceClient.Invoke<CompilerServiceResult>(service => service.Compile(directory, fileName));

            if (compileResult.IsCompiled)
            {
                IdeAlHub.EndCompile(connectionId);
            }
            else
            {
                IdeAlHub.FailCompile(connectionId, compileResult.ErrorList);
            }
        }

        public void ContinueDebug(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryGetValue(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new ContinueCommand()));
        }

        public void StopDebug(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryRemove(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.StopDebugSession(sessionId));
            IdeAlHub.EndStopDebug(connectionId);
        }

        public void StepOverDebug(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryGetValue(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new StepOverCommand()));
        }

        public void StepIntoDebug(string connectionId)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryGetValue(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new StepIntoCommand()));
        }

        public void StartDebug(string connectionId, string fileName, string[] breakpoints)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");
            fileName.ThrowIfNullOrWhiteSpace("fileName");

            var userId = _userIdByConnectionId[connectionId];
            var directory = SrcBaseDirectory + userId;

            var compileResult =
                _serviceClient.Invoke<CompilerServiceResult>(service => service.Compile(directory, fileName));

            if (compileResult.IsCompiled)
            {
                IdeAlHub.EndCompile(connectionId);

                var debugResult = _serviceClient.Invoke(service => service.StartDebugSession(directory, fileName));

                if (debugResult.OperationResult == OperationResult.Started)
                {
                    IdeAlHub.EndStartDebug(connectionId);

                    var sessionId = ((StartedServiceResult)debugResult).SessionId;
                    _connectionIdBySessionId[sessionId] = connectionId;
                    _sessionIdByConnectionId[connectionId] = sessionId;

                    _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new WatchCommand(RegisterGroup.Cpu64)));
                    _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new WatchCommand(RegisterGroup.CpuCtrl)));

                    for (int i = 0; i < breakpoints.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(breakpoints[i]))
                        {
                            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new AddBreakpointCommand(i + 1)));
                        }
                    }

                    _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new StartCommand()));
                }
                else
                {
                    IdeAlHub.FailStartDebug(connectionId);
                }
            }
            else
            {
                IdeAlHub.FailCompile(connectionId, compileResult.ErrorList);
            }
        }

        public void SetBreakpoint(string connectionId, int line)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryGetValue(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new AddBreakpointCommand(line + 1)));
        }

        public void DeleteBreakpoint(string connectionId, int number)
        {
            connectionId.ThrowIfNullOrWhiteSpace("connectionId");

            long sessionId;
            if (!_sessionIdByConnectionId.TryGetValue(connectionId, out sessionId))
                return;

            _serviceClient.Invoke(service => service.ExecuteDebugCommand(sessionId, new DeleteBreakpointCommand(number)));
        }

        private void ProcessServiceEvents()
        {
            KeyValuePair<long, GdbEvent[]> serviceEvent;
            while (_serviceClient.ServiceEventQueue.TryDequeue(out serviceEvent))
            {
                _logger.Debug("ProcessServiceEvent for session: {0}", serviceEvent.Key);

                string connectionId;
                if (!_connectionIdBySessionId.TryGetValue(serviceEvent.Key, out connectionId))
                    continue;

                foreach (var output in serviceEvent.Value)
                {
                    if (output.Type != GdbEventType.ResultRecord)
                        continue;

                    var resultRecord = (ResultRecord)output;
                    _logger.Debug("ServiceEvent: {0}", resultRecord.Class);

                    switch (resultRecord.Class)
                    {
                        case "running":
                            IdeAlHub.OnRunning(connectionId);
                            break;
                        case "break_inserted":
                            OnBreakpointCreated(connectionId, resultRecord.Result);
                            break;
                        case "reg_list":
                            OnRegisterList(connectionId, resultRecord.Result);
                            break;
                        case "var_created":
                            OnVariableCreated(connectionId, resultRecord.Result);
                            break;
                        case "vars_updated":
                            OnVariableUpdated(connectionId, resultRecord.Result);
                            break;
                        case "stopped":
                            OnStopped(connectionId, resultRecord.Result);
                            break;
                        default:
                            _logger.Debug("- ignored");
                            break;
                    }
                }
            }
        }

        private void OnRegisterList(string connectionId, Dictionary<string, GdbValue> output)
        {
            var regGroup = output["group"].Value<string>();
            var registers = output["list"].Value<GdbValue[]>().Select(value => value.Value<string>()).ToArray();

            switch (regGroup)
            {
                case "Cpu64":
                case "Cpu32":
                case "Cpu16":
                case "Cpu8":
                    IdeAlHub.OnRegisterList(connectionId, regGroup, registers);
                    break;
                case "CpuCtrl":
                    IdeAlHub.OnRegisterList(connectionId, registers);
                    break;
            }
        }

        private void OnVariableCreated(string connectionId, Dictionary<string, GdbValue> output)
        {
            var name = output["name"].Value<string>();
            var value = output["value"].Value<string>();

            IdeAlHub.OnVariableCreated(connectionId, name, value);
        }

        private void OnVariableUpdated(string connectionId, Dictionary<string, GdbValue> output)
        {
            var changelist = output["changelist"].Value<GdbValue[]>().Select(value => value.Value<Dictionary<string, GdbValue>>()).ToArray();

            foreach (var element in changelist)
            {
                var name = element["name"].Value<string>();
                var value = element["value"].Value<string>();
                IdeAlHub.OnVariableCreated(connectionId, name, value);
            }
        }

        private void OnBreakpointCreated(string connectionId, Dictionary<string, GdbValue> output)
        {
            var line = output["bkpt"].Value<Dictionary<string, GdbValue>>()["line"].Value<string>();
            var number = output["bkpt"].Value<Dictionary<string, GdbValue>>()["number"].Value<string>();
            IdeAlHub.OnBreakpointCreated(connectionId, line, number);
        }

        private void OnStopped(string connectionId, Dictionary<string, GdbValue> output)
        {
            var reason = output["reason"].Value<string>();
            long sessionId;

            switch (reason)
            {
                case "signal-received":
                    _sessionIdByConnectionId.TryRemove(connectionId, out sessionId);
                    var errName = output["signal-name"].Value<string>();
                    var errMeaning = output["signal-meaning"].Value<string>();
                    var errMessage = string.Format("Программа была завершена с ошибкой {0}. Подробности: {1}.", errName, errMeaning);
                    IdeAlHub.FailRunning(connectionId, errMessage);
                    IdeAlHub.OnStopped(connectionId);
                    break;
                case "exited-normally":
                case "exited":
                    _sessionIdByConnectionId.TryRemove(connectionId, out sessionId);
                    IdeAlHub.OnStopped(connectionId);
                    break;
                case "breakpoint-hit":
                case "end-stepping-range":
                    var line = output["frame"].Value<Dictionary<string, GdbValue>>()["line"].Value<string>();
                    IdeAlHub.OnSelectDebugLine(connectionId, line);
                    IdeAlHub.OnPaused(connectionId);
                    break;
                default:
                    IdeAlHub.OnPaused(connectionId);
                    break;
            }
        }
    }
}
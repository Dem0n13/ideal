using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.AspNet.Identity
{
    public class IdentityRoleMapping : ClassMapping<IdentityRole> 
    {
        public IdentityRoleMapping()
        {
            Table("AspNetRoles");
            Id(role => role.Id, mapper => mapper.Generator(new UUIDHexCombGeneratorDef("D")));
            Property(role => role.Name, mapper => mapper.NotNullable(true));
            Bag(role => role.Users,
                mapper =>
                {
                    mapper.Table("AspNetUserRoles");
                    mapper.Key(k => k.Column("RoleId"));
                },
                relation => relation.ManyToMany(manyToManyMapper => manyToManyMapper.Column("UserId")));
        }
    }
}
﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace NHibernate.AspNet.Identity
{
    public class IdentityRole : IRole
    {
        public IdentityRole(string roleName)
        {
            Name = roleName;
            Users = new List<IdentityUser>();
        }

        public IdentityRole()
        {
        }

        public virtual string Id { get; protected set; }
        public virtual IList<IdentityUser> Users { get; protected set; }
        public virtual string Name { get; set; }
    }
}
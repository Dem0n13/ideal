﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NHibernate.Linq;

namespace NHibernate.AspNet.Identity
{
    /// <summary>
    /// Implements IUserStore using NHibernate where TUser is the entity type of the user being stored
    /// </summary>
    /// <typeparam name="TUser"/>
    public class UserStore<TUser> : IUserLoginStore<TUser>, IUserClaimStore<TUser>, IUserRoleStore<TUser>, IUserPasswordStore<TUser>, IUserSecurityStampStore<TUser>
        where TUser : IdentityUser
    {
        private bool _disposed;
        public ISession Context { get; private set; }

        public UserStore(ISession context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            Context = context;
        }

        public virtual Task<TUser> FindByIdAsync(string userId)
        {
            ThrowIfDisposed();
            return Task.FromResult(Context.Get<TUser>(userId));
        }

        public virtual Task<TUser> FindByNameAsync(string userName)
        {
            ThrowIfDisposed();
            return Transaction(() => Context.Query<TUser>()
                .FirstOrDefault(u => u.UserName.ToUpper() == userName.ToUpper()));
        }

        public virtual Task CreateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Transaction(() => Context.Save(user));
        }

        public virtual Task DeleteAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

             return Transaction(() => Context.Delete(user));
        }

        public virtual Task UpdateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

             return Transaction(() => Context.Flush());
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().Name);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && Context != null)
                Context.Dispose();
            _disposed = true;
            Context = null;
        }

        public virtual Task<TUser> FindAsync(UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (login == null)
                throw new ArgumentNullException("login");

            return Transaction(() => Context.Query<TUser>()
                .SelectMany(user => user.Logins, (u, l) => new {u, l})
                .Where(pair => pair.l.LoginProvider == login.LoginProvider &&
                               pair.l.ProviderKey == login.ProviderKey)
                .Select(pair => pair.u)
                .SingleOrDefault());
        }

        public virtual Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (login == null)
                throw new ArgumentNullException("login");

             return Transaction(() => user.Logins.Add(new IdentityUserLogin
                                                    {
                                                        ProviderKey = login.ProviderKey,
                                                        LoginProvider = login.LoginProvider
                                                    }));
        }

        public virtual Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (login == null)
                throw new ArgumentNullException("login");

            return Transaction(() =>
                               {
                                   var info = user.Logins.SingleOrDefault(x => x.LoginProvider == login.LoginProvider &&
                                                                               x.ProviderKey == login.ProviderKey);
                                   if (info != null)
                                       user.Logins.Remove(info);
                               });
        }

        public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult<IList<UserLoginInfo>>(user.Logins
                .Select(login => new UserLoginInfo(login.LoginProvider, login.ProviderKey))
                .ToList());
        }

        public virtual Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            IList<Claim> result = new List<Claim>();
            foreach (var identityUserClaim in user.Claims)
                result.Add(new Claim(identityUserClaim.ClaimType, identityUserClaim.ClaimValue));

            return Task.FromResult(result);
        }

        public virtual Task AddClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (claim == null)
                throw new ArgumentNullException("claim");

            user.Claims.Add(new IdentityUserClaim
                            {
                                User = user,
                                ClaimType = claim.Type,
                                ClaimValue = claim.Value
                            });

            return Task.FromResult(0);
        }

        public virtual Task RemoveClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (claim == null)
                throw new ArgumentNullException("claim");

            foreach (var identityUserClaim in user.Claims.Where(uc => uc.ClaimValue == claim.Value &&
                                                                      uc.ClaimType == claim.Type).ToList())
            {
                user.Claims.Remove(identityUserClaim);
                Context.Delete(identityUserClaim);
            }

            return Task.FromResult(0);
        }

        public virtual Task AddToRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Value cannot be null or empty", "roleName");
            
            var role = Context.Query<IdentityRole>()
                .SingleOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
            if (role == null)
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "The role {0} not found cannot be null or empty", roleName));

            return Transaction(() => user.Roles.Add(role));
        }

        public virtual Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Value cannot be null or empty", "roleName");

            return Transaction(() =>
                               {
                                   var role = user.Roles
                                       .FirstOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
                                   if (role != null)
                                       user.Roles.Remove(role);
                               });
        }

        public virtual Task<IList<string>> GetRolesAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult<IList<string>>(user.Roles.Select(u => u.Name).ToList());
        }

        public virtual Task<bool> IsInRoleAsync(TUser user, string role)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            if (string.IsNullOrWhiteSpace(role))
                throw new ArgumentException("Value cannot be null or empty", "role");

            return Task.FromResult(user.Roles.Any(r => r.Name.ToUpper() == role.ToUpper()));
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Transaction(() => user.PasswordHash = passwordHash);
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            
            return Task.FromResult(user.PasswordHash);
        }

        public Task SetSecurityStampAsync(TUser user, string securityStamp)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            return Transaction(() => user.SecurityStamp = securityStamp);
        }

        public Task<string> GetSecurityStampAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");
            
            return Task.FromResult(user.SecurityStamp);
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task<IList<TUser>> GetAllAsync()
        {
            return Transaction(() => Context.CreateCriteria<TUser>().List<TUser>());
        }

        protected Task Transaction(System.Action command)
        {
            using (var transaction = Context.BeginTransaction())
            {
                command();
                transaction.Commit();
                return Task.FromResult(0);
            }
        }

        protected Task<TOut> Transaction<TOut>(Func<TOut> command)
        {
            using (var transaction = Context.BeginTransaction())
            {
                var result = command();
                transaction.Commit();
                return Task.FromResult(result);
            }
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace NHibernate.AspNet.Identity
{
    public class IdentityUser : IUser
    {
        public IdentityUser()
        {
            Roles = new List<IdentityRole>();
            Claims = new List<IdentityUserClaim>();
            Logins = new List<IdentityUserLogin>();
        }

        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }

        public virtual string Id { get; protected set; }
        public virtual string UserName { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
        public virtual IList<IdentityRole> Roles { get; protected set; }
        public virtual IList<IdentityUserClaim> Claims { get; protected set; }
        public virtual ICollection<IdentityUserLogin> Logins { get; protected set; }
    }
}
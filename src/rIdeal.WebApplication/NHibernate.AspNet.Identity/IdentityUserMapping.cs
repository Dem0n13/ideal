﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.AspNet.Identity
{
    public class IdentityUserMapping : ClassMapping<IdentityUser>
    {
        public IdentityUserMapping()
        {
            Table("AspNetUsers");
            Id(user => user.Id, m => m.Generator(new UUIDHexCombGeneratorDef("D")));

            Property(user => user.UserName);
            Property(user => user.PasswordHash);
            Property(user => user.SecurityStamp);

            Bag(user => user.Claims,
                mapper => mapper.Key(k => k.Column("User_Id")),
                relation => relation.OneToMany());

            Set(user => user.Logins,
                mapper =>
                {
                    mapper.Table("AspNetUserLogins");
                    mapper.Key(keyMapper => keyMapper.Column("UserId"));
                    mapper.Cascade(Cascade.All);
                },
                relation => relation.Component(componentMapper =>
                                               {
                                                   componentMapper.Property(login => login.LoginProvider);
                                                   componentMapper.Property(login => login.ProviderKey);
                                               }));
            Bag(user => user.Roles,
                mapper =>
                {
                    mapper.Table("AspNetUserRoles");
                    mapper.Key(k => k.Column("UserId"));
                },
                relation => relation.ManyToMany(manyToManyMapper => manyToManyMapper.Column("RoleId")));
        }
    }
}
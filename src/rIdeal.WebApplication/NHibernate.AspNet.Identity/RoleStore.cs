﻿using Microsoft.AspNet.Identity;
using NHibernate.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NHibernate.AspNet.Identity
{
    public class RoleStore<TRole> : IRoleStore<TRole>
        where TRole : IdentityRole
    {
        private bool _disposed;

        public ISession Context { get; private set; }

        public RoleStore(ISession context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            Context = context;
        }

        public Task<TRole> FindByIdAsync(string roleId)
        {
            ThrowIfDisposed();

            return Transaction(() => Context.Get<TRole>(roleId));
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            ThrowIfDisposed();

            return Transaction(() => Context.Query<TRole>().FirstOrDefault(u => u.Name.ToUpper() == roleName.ToUpper()));
        }

        public virtual Task CreateAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
                throw new ArgumentNullException("role");

            return Transaction(() => Context.Save(role));
        }

        public virtual Task DeleteAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
                throw new ArgumentNullException("role");

            return Transaction(() => Context.Delete(role));
        }

        public virtual Task UpdateAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
                throw new ArgumentNullException("role");

            return Transaction(() => Context.Flush());
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().Name);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_disposed)
                Context.Dispose();
            _disposed = true;
            Context = null;
        }

        protected Task Transaction(System.Action command)
        {
            using (var transaction = Context.BeginTransaction())
            {
                command();
                transaction.Commit();
                return Task.FromResult(0);
            }
        }

        protected Task<TOut> Transaction<TOut>(Func<TOut> command)
        {
            using (var transaction = Context.BeginTransaction())
            {
                var result = command();
                transaction.Commit();
                return Task.FromResult(result);
            }
        }
    }
}

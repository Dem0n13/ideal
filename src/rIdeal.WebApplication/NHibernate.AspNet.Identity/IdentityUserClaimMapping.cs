﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.AspNet.Identity
{
    public class IdentityUserClaimMapping : ClassMapping<IdentityUserClaim>
    {
        public IdentityUserClaimMapping()
        {
            Table("AspNetUserClaims");
            Id(x => x.Id, m => m.Generator(Generators.Identity));
            Property(x => x.ClaimType);
            Property(x => x.ClaimValue);
            ManyToOne(x => x.User, m => m.Column("User_Id"));
        }
    }
}